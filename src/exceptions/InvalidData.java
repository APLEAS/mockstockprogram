/*InvalidData.java 
Andrew Pleas
SE 450*/

package exceptions;

public class InvalidData extends Exception {
	


	/**
	 * 
	 */
	private static final long serialVersionUID = 6102601381964672460L;

	/**
	 * InvalidData operation is thrown when in invalid data is used i.e. (a null user name) 
	 */
	public InvalidData(String error) {
		super(error);
	}
}


