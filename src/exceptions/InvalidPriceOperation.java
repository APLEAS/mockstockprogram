/*InvalidPiceOperation.java 
Andrew Pleas
SE 450*/
package exceptions;

/**
 * InvalidPrice operation is thrown to prevent illegal Price operations from being preformed i.e. 
 * (adding a market value to a limit value)
 */
public class InvalidPriceOperation extends Exception {


	/**
	 * 
	 */
	private static final long serialVersionUID = -811088114037250182L;

	public InvalidPriceOperation(String error) {
		super(error);
	}
}
