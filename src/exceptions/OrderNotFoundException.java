package exceptions;

public class OrderNotFoundException extends Exception{


	/**
	 * 
	 */
	private static final long serialVersionUID = -4701240978574379646L;

	/**
	 * 
	 */

	/**
	 * InvalidData operation is thrown when in invalid data is used i.e. (a null user name) 
	 */
	public OrderNotFoundException(String error) {
		super(error);
	}
}

