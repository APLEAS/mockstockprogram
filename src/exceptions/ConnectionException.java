/*InvalidData.java 
Andrew Pleas
SE 450*/

package exceptions;

public class ConnectionException extends Exception {
	


	/**
	 * 
	 */

	private static final long serialVersionUID = -3372527732718553833L;

	/**
	 * InvalidData operation is thrown when in invalid data is used i.e. (a null user name) 
	 */
	public ConnectionException(String error) {
		super(error);
	}
}

