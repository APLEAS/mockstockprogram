package user;

import java.util.ArrayList;

import enums.BookSide;
import exceptions.ConnectionException;
import exceptions.InvalidData;
import exceptions.InvalidPriceOperation;
import exceptions.OrderNotFoundException;
import messages.CancelMessage;
import messages.FillMessage;
import price.Price;
import tradable.TradableDTO;

public interface User {
	String getUserName();
	
	void acceptLastSale(String product, Price p, int v);
	
	void acceptMessage(FillMessage fm) throws InvalidPriceOperation, InvalidData;
	
	void acceptMessage(CancelMessage cm) throws InvalidData;
	
	void acceptMarketMessage(String message) throws InvalidData;
	
	void acceptTicker( String product, Price p, char direction) throws InvalidData;

	void acceptCurrentMarket(String product, Price bp, int bv, Price sp, int sv);
	void connect() throws ConnectionException, InvalidData;
	void disConnect() throws ConnectionException, InvalidData;
	void showMarketDisplay() throws Exception;
	void submitOrderCancel(String product, BookSide side, String orderId) throws InvalidData, OrderNotFoundException, ConnectionException;
	void submitQuote(String product, Price buyPrice, int buyVolume, Price sellPrice, int sellVolume) throws Exception;
	void submitQuoteCancel(String product) throws InvalidData, ConnectionException;
	void subscribeCurrentMarket(String product) throws ConnectionException, InvalidData;
	void subscribeLastSale(String product) throws ConnectionException, InvalidData;
	void subscribeMessages(String product) throws ConnectionException, InvalidData;
	void subscribeTicker(String product) throws ConnectionException, InvalidData;
	Price getAllStockValue() throws InvalidPriceOperation, InvalidData;
	Price getAccountCosts();
	Price getNetAccountValue() throws InvalidPriceOperation, InvalidData;
	String[][] getBookDepth(String product) throws InvalidData, ConnectionException;
	String getMarketState() throws ConnectionException;
	ArrayList<TradableUserData> getOrderIds();
	ArrayList<String> getProductList();
	Price getStockPositionValue(String sym) throws InvalidPriceOperation, InvalidData;
	int getStockPositionVolume(String product) throws InvalidData;
	ArrayList<String> getHoldings() throws InvalidData;
	ArrayList<TradableDTO> getOrdersWithRemainingQty(String product) throws InvalidData, ConnectionException;

	String submitOrder(String product, Price price, int volume, BookSide side) throws Exception;
	
	

	
	
	

}
