package user;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import price.Price;
import publisher.CurrentMarketPublisher;
import publisher.LastSalePublisher;
import publisher.MessagePublisher;
import publisher.PublisherFactory;
import publisher.TickerPublisher;
import tradable.Order;
import tradable.Quote;
import tradable.TradableDTO;
import enums.BookSide;
import exceptions.ConnectionException;
import exceptions.InvalidData;
import exceptions.OrderNotFoundException;
import books.ProductService;

public class UserCommandService {
	private static HashMap<String, Long> connectedUserIds = new HashMap<String, Long>();
	private static HashMap<String, User> connectedUsers = new HashMap<String, User>();
	private static HashMap<String, Long> connectedTime = new HashMap<String, Long>(); 
	private static volatile UserCommandService instance;

	private UserCommandService() {
		// TODO Auto-generated constructor stub
	}

	public synchronized static UserCommandService getInstance(){
		if(instance ==null){
			instance = new UserCommandService();
			return instance;
		}
		else
			return instance;
	}

	private void verifyUser(String userName, long connId)throws ConnectionException{
		if(connectedUserIds == null)
			throw new ConnectionException("ConnectionException: (UserCommandService verifyUser) connectedUserIds is null");
		if(!connectedUserIds.containsKey(userName)){
			throw new ConnectionException("ConnectionException: (UserCommandService verifyUser) User is not connected");
		}else if (connectedUserIds.get(userName).equals(userName)){
			throw new ConnectionException("ConnectionException: (UserCommandService verifyUser) User name does not match this user id");
		}else return;
	}

	public synchronized long connect(User user) throws ConnectionException{
		if(connectedUserIds.containsKey(user.getUserName())){
			throw new ConnectionException("ConnectionException: (UserCommandService connect) User is allready connected");
		}
		else { 
			long time = System.nanoTime();
			connectedUserIds.put(user.getUserName(), time);
			connectedUsers.put(user.getUserName(), user);
			connectedTime.put(user.getUserName(), System.nanoTime());
			return connectedUserIds.get(user.getUserName());
		}

	}

	public synchronized void disConnect(String userName, long connId) throws ConnectionException, InvalidData {
		if(userName == null || userName.isEmpty())
			throw new InvalidData("InvalidData: (UserCommandService disConnect) userName cannot be null");
		verifyUser(userName, connId);
		connectedUserIds.remove(userName);
		connectedUsers.remove(userName);
		connectedTime.remove(userName);
	}

	public String[][] getBookDepth(String userName, long connId, String product) throws InvalidData, ConnectionException {
		verifyUser(userName, connId);
		return ProductService.getInstance().getBookDepth(product);

	}

	public String getMarketState(String userName, long connId) throws ConnectionException {
		verifyUser(userName, connId);
		return ProductService.getInstance().getMarketState().toString();
	}

	public synchronized ArrayList<TradableDTO> getOrdersWithRemainingQty(String userName, long connId, String product) throws InvalidData, ConnectionException{
		verifyUser(userName, connId);
		return ProductService.getInstance().getOrdersWithRemainingQty(userName, product);
	}

	public ArrayList<String> getProducts(String userName, long connId) throws ConnectionException{
		verifyUser(userName, connId);
		ArrayList<String> pList = ProductService.getInstance().getProductList();
		Collections.sort(pList);
		return pList;
	}

	public String submitOrder(String userName, long connId, String product, Price price, int volume, BookSide side) throws Exception{
		verifyUser(userName, connId);
		Order o = new Order(userName, product, price, volume, side);
		String orderId = ProductService.getInstance().submitOrder(o);
		return orderId;
	}

	public void submitOrderCancel(String userName, long connId, String product, BookSide side, String orderId) throws InvalidData, OrderNotFoundException, ConnectionException {
		verifyUser(userName, connId);
		ProductService.getInstance().submitOrderCancel(product, side, orderId);
	}

	public void submitQuote(String userName, long connId, String product, Price bPrice, int bVolume, Price sPrice, int sVolume) throws Exception{
		verifyUser(userName, connId);
		Quote q = new Quote(userName, product, bPrice, bVolume, sPrice, sVolume);
		ProductService.getInstance().submitQuote(q);
	}

	public void submitQuoteCancel(String userName, long connId, String product) throws InvalidData, ConnectionException{
		verifyUser(userName, connId);
		ProductService.getInstance().submitQuoteCancel(userName, product);
	}

	public void subscribeCurrentMarket(String userName, long connId, String product) throws ConnectionException {
		verifyUser(userName, connId);
		CurrentMarketPublisher.getInstance().subscribe(connectedUsers.get(userName), product);

	}

	public void subscribeLastSale(String userName, long connId, String product) throws ConnectionException {
		verifyUser(userName, connId);
		LastSalePublisher.getInstance().subscribe(connectedUsers.get(userName), product);
	}

	public void subscribeMessages(String userName, long conn, String product) throws ConnectionException {
		verifyUser(userName, conn);
		MessagePublisher.getInstance().subscribe(connectedUsers.get(userName), product);

	}

	public void subscribeTicker(String userName, long conn, String product) throws ConnectionException {
		verifyUser(userName, conn);
		TickerPublisher.getInstance().subscribe(connectedUsers.get(userName), product);
	}

	public void unSubscribeCurrentMarket(String userName, long conn, String product) throws InvalidData, ConnectionException {
		verifyUser(userName, conn);
		CurrentMarketPublisher.getInstance().unSubscribe(connectedUsers.get(userName), product);
	}

	public void unSubscribeLastSale(String userName, long conn, String product) throws InvalidData, ConnectionException{
		verifyUser(userName, conn);
		LastSalePublisher.getInstance().unSubscribe(connectedUsers.get(userName), product);
	}

	public void unSubscribeTicker(String userName, long conn, String product) throws InvalidData, ConnectionException {
		verifyUser(userName, conn);
		TickerPublisher.getInstance().unSubscribe(connectedUsers.get(userName), product);
	}

	public void unSubscribeMessages(String userName, long conn, String product) throws InvalidData, ConnectionException {
		verifyUser(userName, conn);
		MessagePublisher.getInstance().unSubscribe(connectedUsers.get(userName), product);
	}











}
