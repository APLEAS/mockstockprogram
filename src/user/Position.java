package user;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import enums.BookSide;
import exceptions.InvalidData;
import exceptions.InvalidPriceOperation;
import price.Price;

public class Position {

	private HashMap<String, Integer> holdings;
	private Price accountCosts;
	private HashMap<String, Price> lastSales;
	public Position() {
		accountCosts = price.PriceFactory.makeLimitPrice(0);
		lastSales = new HashMap<>();
		holdings = new HashMap<>();

	}
	public void updatePosition(String product, Price price, BookSide side, int volume) throws InvalidPriceOperation, InvalidData{
		if(product == null || price == null || side == null)
			throw new InvalidData("InvalidData: (Position updatePosition) Produce, price or side cannot be null");
		if (product.isEmpty())
			throw new InvalidData("InvalidData: (Position updatePosition) Produce cannot be empty");
			
		int adjustedVolume = 0;
		adjustedVolume = volume;
		if(side == BookSide.SELL)
			adjustedVolume = -1*volume;
		if(!holdings.containsKey(product)){
			holdings.put(product, adjustedVolume);
		}else
		{
			int adjust = holdings.get(product);
			adjust += adjustedVolume;
			if (adjust != 0){
				holdings.put(product, adjust);
			}else holdings.remove(product);
		}
		Price adj = price.multiply(volume);
		if(side == BookSide.BUY){
			accountCosts = accountCosts.subtract(adj);
		}else if(side == BookSide.SELL)
			accountCosts = accountCosts.add(adj);

	}

	public void updateLastSale(String product, Price price) throws InvalidData {
		if(product == null || price == null) 
			throw new InvalidData("InvalidData: (Position updateLastSale) Produce or price cannot be null");
		if (product.isEmpty())
			throw new InvalidData("InvalidData: (Position updateLastSale) Produce cannot be empty");
		lastSales.put(product, price);
	}
	

	public int getStockPositionVolume(String product) throws InvalidData{
		if(product == null) 
			throw new InvalidData("InvalidData: (Position getStockPositionVolume) Produce or price cannot be null");
		if (product.isEmpty())
			throw new InvalidData("InvalidData: (Position getStockPositionVolume) Produce cannot be empty");
		if(!holdings.containsKey(product))
			return 0;
		else return holdings.get(product);
	}

	public ArrayList<String> getHoldings() throws InvalidData {
		if(holdings == null) 
			throw new InvalidData("InvalidData: (Position getHoldings) Holdings cannot be null or empty");
		ArrayList<String> h = new ArrayList<>(holdings.keySet()); 
		Collections.sort(h);
		return h;
	}

	public Price getStockPositionValue(String product) throws InvalidPriceOperation, InvalidData{
		if(product == null || product.isEmpty()) 
			throw new InvalidData("InvalidData: (Position getStockPositionValue) product cannot be null or empty");
		if(!holdings.containsKey(product) || lastSales.get(product) == null){
			return new Price(0);
		}else{
			Price last = lastSales.get(product);
			return last.multiply(holdings.get(product));
		}
	}
	public Price getAccountCosts(){
		return accountCosts;
	}


	public Price getAllStockValue() throws InvalidPriceOperation, InvalidData{
		ArrayList <String> h = new ArrayList<String>(holdings.keySet());
		long sum = 0;
		for (String s : h){
			Price p = getStockPositionValue(s);
			sum += p.getPrice();

		}Price summary = new Price(sum); 
		return summary;
	}


	public Price getNetAccountValue() throws InvalidPriceOperation, InvalidData {
		return getAllStockValue().add(accountCosts);
	}



}
