package user;

import enums.BookSide;
import exceptions.InvalidData;

public class TradableUserData {
private String userName;
private String orderId;
private String stockSymbol;
private BookSide side;

	public TradableUserData(String userNameIn, String orderIdIn, String stockSymbolIn, BookSide sideIn) throws InvalidData {
		setUserName(userNameIn);
		setOrderId(orderIdIn);
		setStockSymbol(stockSymbolIn);
		setSide(sideIn);
		
	}

	private void setSide(BookSide sideIn) throws InvalidData {
		if(sideIn == null)
			throw new InvalidData("InvalidData: (TradableUserData setSide) Side cannot be null");
		side = sideIn;
		
	}

	private void setStockSymbol(String stockSymbolIn) throws InvalidData {
		if(stockSymbolIn == null)
			throw new InvalidData("InvalidData: (TradableUserData setStockSymbol) stockSymbol cannot be null");
		if(stockSymbolIn.isEmpty())
			throw new InvalidData("InvalidData: (TradableUserData setStockSymbol) stockSymbol cannot be empty");
		stockSymbol = stockSymbolIn;
		
	}
	
    public String getProduct() {
        return stockSymbol;
    }

	public String getUserName(){
		return userName;
	}
	
	public String getOrderId(){
		return orderId;
	}
	
	public String getUserStockSymbol(){
		return stockSymbol;
	}
	
	public BookSide getSide(){
		return side;
	}
	
	private void setUserName(String newUser) throws InvalidData{
		if(newUser == null)
			throw new InvalidData("InvalidData: (TradableUserData setUserName) newUser cannot be null");
		if(newUser.isEmpty())
			throw new InvalidData("InvalidData: (TradableUserData setUserName) newUser cannot be empty");
		userName = newUser;
	}
	
	private void setOrderId(String newOrderId) throws InvalidData{
		if(newOrderId == null)
			throw new InvalidData("InvalidData: (TradableUserData setOrderId) newOrderId cannot be null");
		if(newOrderId.isEmpty())
			throw new InvalidData("InvalidData: (TradableUserData setOrderId) newOrderId cannot be empty");
		orderId = newOrderId;
	}
	
	public String toString(){
		String message = "User " + userName + ", " + side + " " + stockSymbol + "(" + orderId + ")";
		return message;
	}
	
	
	
}
