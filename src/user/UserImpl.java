package user;

import java.util.ArrayList;
import java.sql.Timestamp;

import enums.BookSide;
import exceptions.ConnectionException;
import exceptions.InvalidData;
import exceptions.InvalidPriceOperation;
import exceptions.OrderNotFoundException;
import gui.UserDisplayManager;
import messages.CancelMessage;
import messages.FillMessage;
import price.Price;
import tradable.TradableDTO;

public class UserImpl implements User {

    private String userName;
    private long connectionId;
    private ArrayList<String> availableStocks = new ArrayList();
    private ArrayList<TradableUserData> orders = new ArrayList();
    private Position position;
    private UserDisplayManager udm; 
    
    

    public UserImpl(String u) throws InvalidData {
        position = new Position();
        setUserName(u);
        setPosition();
    }

    private void setPosition() {
		position = new Position();
		
	}

	private void setUserName(String u) throws InvalidData {
		if (u == null)
			throw new InvalidData("InvalidData: (UserImpl serUser) User Name cannot be null");
		if (u.isEmpty())
			throw new InvalidData("InvalidData: (UserImpl serUser) User Name cannot be empty");
		userName = u;
		
	}

	@Override
    public String getUserName() {
        return userName;
    }

    @Override
    public void acceptLastSale(String product, Price p, int v) {
    	try{
    		if(udm !=null){
    	udm.updateLastSale(product, p, v);
    		}
    	position.updateLastSale(product, p);
    	}catch (Exception ex) {
            System.out.println("Exception properly caught error: " + ex.getMessage());
            System.out.println();
        }
       // System.out.println("User " + getUserName() + " Received Last Sale for " + product + " " + v + "@" + p);
    }

    @Override
    public void acceptMessage(FillMessage fm) throws InvalidPriceOperation, InvalidData {
		if (fm == null)
			throw new InvalidData("InvalidData: (UserImpl acceptMessage) Fill message cannot be null");
    	Timestamp t = new Timestamp(System.currentTimeMillis());
    		StringBuilder message = new StringBuilder();
    		message.append(String.format("(%s) FillMessage: %s %s %s at %s %s [Tradable Id: %s] ",t, fm.getSide().toString(), fm.getVolume(), fm.getProduct(), fm.getPrice(), fm.getDetails(), fm.getId()));
    		try{
    			if(udm !=null){
    	udm.updateMarketActivity(message.toString());
    			}
    	position.updatePosition(fm.getProduct(), fm.getPrice(), fm.getSide(), fm.getVolume());
	}catch (Exception ex) {
        System.out.println("Exception properly caught error: " + ex.getMessage());
        System.out.println();
    }
    }

    @Override
    public void acceptMessage(CancelMessage cm) throws InvalidData {
		if (cm == null)
			throw new InvalidData("InvalidData: (UserImpl acceptMessage) Cancel message cannot be null");
    	Timestamp t = new Timestamp(System.currentTimeMillis());
		StringBuilder message = new StringBuilder();
		message.append(String.format("(%s) FillMessage: %s %s %s at %s %s [Tradable Id: %s] ",t, cm.getSide().toString(), cm.getVolume(), cm.getProduct(), cm.getPrice(), cm.getDetails(), cm.getId()));
  		try{
  			if(udm !=null){
  	    	udm.updateMarketActivity(message.toString());
  			}
  		}catch (Exception ex) {
  	        System.out.println("Exception properly caught error: " + ex.getMessage());
  	        System.out.println();
  	    }
    }

    @Override
    public void acceptMarketMessage(String message) throws InvalidData {
		if (message == null)
			throw new InvalidData("InvalidData: (UserImpl acceptMarketMessage) Message cannot be null");
		if (message.isEmpty())
			throw new InvalidData("InvalidData: (UserImpl acceptMarketMessage) Message cannot be empty");
    	try{
    		if(udm !=null){
    	udm.updateMarketState(message);
    		}
  		}catch (Exception ex) {
  	        System.out.println("Exception properly caught error: " + ex.getMessage());
  	        System.out.println();
  	    }
    }

    @Override
    public void acceptTicker(String product, Price p, char direction) throws InvalidData {
		if (product == null || p == null)
			throw new InvalidData("InvalidData: (UserImpl acceptTicker) product or price cannot be null");
		if (product.isEmpty())
			throw new InvalidData("InvalidData: (UserImpl acceptTicker) Product cannot be empty");
    	try{
    		if(udm !=null){
    	udm.updateTicker(product, p, direction);
    		}
  		}catch (Exception ex) {
  	        System.out.println("Exception properly caught error: " + ex.getMessage());
  	        System.out.println();
  	    }
    }

    @Override
    public void acceptCurrentMarket(String product, Price bp, int bv, Price sp, int sv) {
    	try{
    		if(udm !=null){
    	udm.updateMarketData(product, bp, bv, sp, sv);;
    		}
  		}catch (Exception ex) {
  	        System.out.println("Exception properly caught error: " + ex.getMessage());
  	        System.out.println();
  	    }
    }
    

	@Override
	//need to propegate these exceptions !!!!!!
	public void connect() throws ConnectionException, InvalidData {
	connectionId = UserCommandService.getInstance().connect(this);
	if( UserCommandService.getInstance().getProducts(userName, connectionId) == null)
		throw new InvalidData("InvalidData: (UserImpl  connect) UserCommandService getProduct returned null");
	availableStocks = UserCommandService.getInstance().getProducts(userName, connectionId);	
	}

	@Override
	//need to propegate these exceptions !!!!!!
	public void disConnect() throws ConnectionException, InvalidData {
	UserCommandService.getInstance().disConnect(userName, connectionId);	
	}

	@Override
	public void showMarketDisplay() throws Exception {
		if(availableStocks == null)
			throw new InvalidData("InvalidData: avalable stock list is empty");
		if(udm == null){
			udm = new UserDisplayManager(this);
			udm.showMarketDisplay();
		}
		
	}
	@Override
	 public String submitOrder(String product, Price price, int volume, BookSide side) throws Exception{
		if (product == null || price == null || side == null)
			throw new InvalidData("InvalidData: (UserImpl submitOrder) product, price or side cannot be null");
		if (product.isEmpty())
			throw new InvalidData("InvalidData: (UserImpl submitOrder) Product cannot be empty");	
		
	String s = UserCommandService.getInstance().submitOrder(userName, connectionId, product, price, volume, side);
	TradableUserData t = new TradableUserData(userName, s, product, side);
	orders.add(t);
		return s;
	}
	

	@Override
	//propegate
	public void submitOrderCancel(String product, BookSide side, String orderId) throws InvalidData, OrderNotFoundException, ConnectionException{
		if (product == null || side == null || orderId == null)
			throw new InvalidData("InvalidData: (UserImpl submitOrder) product, OrderId or side cannot be null");
		if (product.isEmpty() || orderId.isEmpty())
			throw new InvalidData("InvalidData: (UserImpl submitOrder) Product or orderId cannot be empty");
		UserCommandService.getInstance().submitOrderCancel(userName, connectionId, product, side, orderId);
		
	}

	
	public void submitQuote(String product, Price buyPrice, int buyVolume,
			Price sellPrice, int sellVolume) throws Exception {
		if (product == null || buyPrice == null || sellPrice == null)
			throw new InvalidData("InvalidData: (UserImpl submitQuote) product, buyPrice or sellPrice cannot be null");
		if (product.isEmpty())
			throw new InvalidData("InvalidData: (UserImpl submitQuote) Product cannot be empty");
		
		UserCommandService.getInstance().submitQuote(userName, connectionId, product, buyPrice, buyVolume, sellPrice, sellVolume);
	}

	@Override
	public void submitQuoteCancel(String product) throws InvalidData, ConnectionException {
		if (product == null)
			throw new InvalidData("InvalidData: (UserImpl submitQuoteCancel) product cannot be null");
		if (product.isEmpty())
			throw new InvalidData("InvalidData: (UserImpl submitQuoteCancel) Product cannot be empty");
		UserCommandService.getInstance().submitQuoteCancel(userName, connectionId, product);
		
		
	}

	@Override
	public void subscribeCurrentMarket(String product) throws ConnectionException, InvalidData {
		if (product == null)
			throw new InvalidData("InvalidData: (UserImpl subscribeCurrentMarket) product cannot be null");
		if (product.isEmpty())
			throw new InvalidData("InvalidData: (UserImpl subscribeCurrentMarket) Product cannot be empty");
		UserCommandService.getInstance().subscribeCurrentMarket(userName, connectionId, product);
	}

	@Override
	public void subscribeLastSale(String product) throws ConnectionException, InvalidData {
		if (product == null)
			throw new InvalidData("InvalidData: (UserImpl subscribeLastSale) product cannot be null");
		if (product.isEmpty())
			throw new InvalidData("InvalidData: (UserImpl subscribeLastSale) Product cannot be empty");
		UserCommandService.getInstance().subscribeLastSale(userName, connectionId, product);	
	}

	@Override
	public void subscribeMessages(String product) throws ConnectionException, InvalidData {
		if (product == null)
			throw new InvalidData("InvalidData: (UserImpl subscribeMessages) product cannot be null");
		if (product.isEmpty())
			throw new InvalidData("InvalidData: (UserImpl subscribeMessages) Product cannot be empty");
		UserCommandService.getInstance().subscribeMessages(userName, connectionId, product);
		
	}

	@Override
	public void subscribeTicker(String product) throws ConnectionException, InvalidData {
		if (product == null)
			throw new InvalidData("InvalidData: (UserImpl subscribeTicker) product cannot be null");
		if (product.isEmpty())
			throw new InvalidData("InvalidData: (UserImpl subscribeTicker) Product cannot be empty");
		UserCommandService.getInstance().subscribeTicker(userName, connectionId, product);
		
	}

	@Override
	public Price getAllStockValue() throws InvalidPriceOperation, InvalidData {
		return position.getAllStockValue();
		
	}

	@Override
	public Price getAccountCosts() {
		return position.getAccountCosts();
	}

	@Override
	public Price getNetAccountValue() throws InvalidPriceOperation, InvalidData {
		return position.getNetAccountValue();
	}

	@Override
	public String[][] getBookDepth(String product) throws InvalidData, ConnectionException {
		return UserCommandService.getInstance().getBookDepth(userName, connectionId, product);
	}

	@Override
	public String getMarketState() throws ConnectionException {
		// TODO Auto-generated method stub
		return UserCommandService.getInstance().getMarketState(userName, connectionId);
	}

	@Override
	public ArrayList<TradableUserData> getOrderIds() {
		// TODO Auto-generated method stub
		return orders;
	}

	@Override
	public ArrayList<String> getProductList() {
		ArrayList<String> stocks = availableStocks;
		return stocks;
	}

	@Override
	public Price getStockPositionValue(String product) throws InvalidPriceOperation, InvalidData {
		// TODO Auto-generated method stub
		return position.getStockPositionValue(product);
	}

	@Override
	public int getStockPositionVolume(String product) throws InvalidData {
		return position.getStockPositionVolume(product);
	}

	@Override
	public ArrayList<String> getHoldings() throws InvalidData {
		return position.getHoldings();
	}

	@Override
	public ArrayList<TradableDTO> getOrdersWithRemainingQty(String product) throws InvalidData, ConnectionException {
		return UserCommandService.getInstance().getOrdersWithRemainingQty(userName, connectionId, product);
	}
}
