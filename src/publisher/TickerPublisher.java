/*TickerPublisher.java 
Andrew Pleas
SE 450*/

package publisher;
import java.util.ArrayList;
import java.util.HashMap;

import exceptions.InvalidData;
import price.Price;
import price.PriceFactory;
import user.User;
public class TickerPublisher extends PublisherImpl {
	private static PublisherImpl publisher;
	private static volatile TickerPublisher instance;
	private HashMap<String, Price> ticker = new HashMap<>();

	public TickerPublisher() {}

	//returns an instance of TickerPublisher
	public static TickerPublisher getInstance(){
		if(instance ==null){
			instance = new TickerPublisher();
			publisher = PublisherFactory.makePublisher();
			return instance;
		}
		else
			return instance;
	}
	/*subscribes users to a product
	 * @param User u (the users name)
	 * @param String product (the stock symbol)*/
	@Override
	public synchronized void subscribe(User u, String product) {
		if (publisher != null) {
			publisher.subscribe(u, product);
		}
	}
	/*unsubscribes users to a product
	 * @param User u (the users name)
	 * @param String product (the stock symbol)*/
	@Override
	public synchronized void unSubscribe(User u, String product) throws InvalidData {
		if (publisher != null) {
			publisher.unSubscribe(u, product);
		}
	}

	/*publishes a ticker message to Users subscribed to a particular stock (product)
	 * Stores the product and old price in a HashMap (ticker).
	 * The new Price is compared with the new price to determine the direction the stock is going
	 * @param String product (the stock symbol)
	 * @param Price p (the new price for the stock)  */

	public synchronized void publishTicker(String product, Price p) throws InvalidData {
		if (p == null){
			p =  PriceFactory.makeLimitPrice("0.00");
		}
		Price change;
		if(!(publisher.getSubscribers().isEmpty())){
			char direction = ' ';

			if((ticker.containsKey(product))){
				change = ticker.get(product);

				int compare = change.compareTo(p);
				if(compare < 0)
					direction = ((char)8593);
				if(compare > 0)
					direction = ((char)8595);
				if (compare == 0)
					direction = '=';
				ticker.put(product, p);
			}

			if(!ticker.containsKey(product))
				ticker.put(product, p);

			if (publisher.getSubscribers().size() == 0){
				throw new InvalidData("InvalidData: There are no subscribers");	
			}
			if (!publisher.getSubscribers().isEmpty()){
				HashMap<String, ArrayList<User>> sub = publisher.getSubscribers();
				if (!(sub.get(product) == null)){
					ArrayList <User> u = sub.get(product);
					if (u.size()>0)
						for (User user: u)
							user.acceptTicker(product, p, direction);

				}
			}

		}
	}
}
