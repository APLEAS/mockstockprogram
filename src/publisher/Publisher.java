/*Publisher.java 
Andrew Pleas
SE 450*/

package publisher;

import exceptions.InvalidData;
import user.User;



public interface Publisher {
	
	/*subscribes users to a product
	 * @param User u (the users name)
	 * @param String product (the stock symbol)*/
	void subscribe(User user, String product);
	
	/*unsubscribes users to a product
	 * @param User u (the users name)
	 * @param String product (the stock symbol)*/
	void unSubscribe(User user, String product) throws InvalidData;


}
