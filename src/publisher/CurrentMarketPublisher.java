/*CurrentMarketPublisher.java 
Andrew Pleas
SE 450*/

package publisher;

import java.util.ArrayList;
import java.util.HashMap;

import exceptions.InvalidData;
import user.User;
import messages.MarketDataDTO;
import publisher.PublisherImpl;

public class CurrentMarketPublisher extends PublisherImpl {
	private static PublisherImpl publisher;
	private static volatile CurrentMarketPublisher instance;

	private CurrentMarketPublisher(){
	}

	//returns an instance of CurrentMarketPublisher
	public static CurrentMarketPublisher getInstance(){
		if(instance ==null){
			instance = new CurrentMarketPublisher();
			publisher = PublisherFactory.makePublisher();
			return instance;
		}
		else
			return instance;
	}
	/*subscribes users to a product
	 * @param User u (the users name)
	 * @param String product (the stock symbol)*/
	@Override
	public synchronized void subscribe(User u, String product){
		if (publisher != null) {
			publisher.subscribe(u, product);
		}
	}
	/*unsubscribes users to a product
	 * @param User u (the users name)
	 * @param String product (the stock symbol)*/
	@Override
	public synchronized void unSubscribe(User u, String product) throws InvalidData {
		if (publisher != null) {
			publisher.unSubscribe(u, product);
		}
	}

	/*synchronized method that publishes current current market data to users 
	 * subscribed to a particular product
	 * @param MarketDTO md (MarketDataDTO Object used to get Market data)*/
	
	public synchronized void publishCurrentMarket(MarketDataDTO md){
		if (publisher.getSubscribers().size() == 0){
			//figure out what to do if list is empty make an error
		}
		if (!publisher.getSubscribers().isEmpty()){
			HashMap<String, ArrayList<User>> sub = publisher.getSubscribers();
			if (!(sub.get(md.product) == null)){
				ArrayList <User> u = sub.get(md.product);
				if(u.size()>0)
					for (User user : u){
						user.acceptCurrentMarket(md.product, md.buyPrice, md.buyVolume, md.sellPrice, md.sellVolume);
					}
			}

		}

	}

}


