/*LastSalePublisher.java 
Andrew Pleas
SE 450*/

package publisher;

import java.util.ArrayList;
import java.util.HashMap;

import exceptions.InvalidData;
import price.Price;
import price.PriceFactory;
import user.User;

public class LastSalePublisher extends PublisherImpl {

	private static PublisherImpl publisher;
	private static volatile LastSalePublisher instance;
	public LastSalePublisher () {

	}
	//returns an instance of LastSalePublisher
	public static LastSalePublisher getInstance(){
		if(instance ==null){
			instance = new LastSalePublisher();
			publisher = PublisherFactory.makePublisher();
			return instance;
		}
		else
			return instance;
	}
	/*subscribes users to a product
	 * @param User u (the users name)
	 * @param String product (the stock symbol)*/
	@Override
	public synchronized void subscribe(User u, String product) {
		if (publisher != null) {
			publisher.subscribe(u, product);
		}
	}
	/*unsubscribes users to a product
	 * @param User u (the users name)
	 * @param String product (the stock symbol)*/
	@Override
	public synchronized void unSubscribe(User u, String product) throws InvalidData {
		if (publisher != null) {
			publisher.unSubscribe(u, product);
		}
	}

	/*Publishes a Last sale message to everyone subscribed to a particular stock
	 * @param String product (the stock symbol)
	 * @param Price p (price of the last sale)
	 * @param int v (volume of the last sale)
	 * */
	public synchronized void publishLastSale(String product, Price p, int v) throws InvalidData{
		if (p == null){
			p =  PriceFactory.makeLimitPrice("0.00");
		}

		if (publisher.getSubscribers().size() == 0){
			throw new InvalidData("InvalidData: Subscriber list is empty");	
		}
		if (!publisher.getSubscribers().isEmpty()){
			HashMap<String, ArrayList<User>> sub = publisher.getSubscribers();
			if (!(sub.get(product) == null)){
				ArrayList <User> u = sub.get(product);

				if(u.size()>0){
					for (User user : u){
						user.acceptLastSale(product, p, v);
					}
					TickerPublisher.getInstance().publishTicker(product, p);
				}
			}
		}
	}
}
