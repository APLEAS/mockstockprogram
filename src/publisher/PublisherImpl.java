/*PublisherImpl.java 
Andrew Pleas
SE 450*/

package publisher;

import java.util.ArrayList;
import java.util.HashMap;

import exceptions.InvalidData;
import user.User;



public class PublisherImpl implements Publisher {

	//stores an ArrayList of Users for each stock 
	private HashMap<String, ArrayList<User>> subscribers = new HashMap<>();
	//Stores all the subscribers of any stock
	private ArrayList<User> users = new ArrayList<User>();


	public PublisherImpl() {
		//subscribers = new HashMap<>();
	}

	/*subscribes users to a product
	 * adds user to Users ArrayList
	 * @param User u (the users name)
	 * @param String product (the stock symbol)*/
	public synchronized void subscribe(User user, String product){		
		ArrayList<User> subsc = new ArrayList<User>();	
		users.add(user);
		if(subscribers.containsKey(product)){
			subscribers.get(product).add(user); 
		}
		if(!subscribers.containsKey(product)){
			subsc.add(user);
			subscribers.put(product, subsc);
		}	
	}

	/*unsubscribes users to a product
	 * removes user from Users ArrayList
	 * @param User u (the users name)
	 * @param String product (the stock symbol)*/
	public synchronized void unSubscribe (User user, String product) throws InvalidData{		
		users.remove(user);
		if(!(subscribers.containsKey(product))){
			throw new InvalidData("Invalid Data: User is not subsribed to this stock");	
		}
		if(subscribers.containsKey(product)){
			subscribers.get(product).remove(user); 
		}

	}
	//returns a hashMap of all the subscribers 
	public synchronized  HashMap<String, ArrayList<User>> getSubscribers() {
		HashMap<String, ArrayList<User>> subscribe = (HashMap<String, ArrayList<User>>)subscribers.clone(); 
		return subscribe;
	}

	//returns an ArrayList of all the Users
	public synchronized ArrayList<User> getUsers(){
		ArrayList<User> user = (ArrayList<User>)users.clone(); 
		return user;
	}







}
