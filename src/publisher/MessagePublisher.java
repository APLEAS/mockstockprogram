/*MessagePublisher.java 
Andrew Pleas
SE 450*/

package publisher;

import java.util.ArrayList;
import java.util.HashMap;

import exceptions.InvalidData;
import exceptions.InvalidPriceOperation;
import user.User;
import messages.CancelMessage;
import messages.FillMessage;
import messages.MarketMessage;

public class MessagePublisher {

	private static PublisherImpl publisher;
	private static volatile MessagePublisher instance;

	public MessagePublisher() {
	}

	/*subscribes users to a product
	 * @param User u (the users name)
	 * @param String product (the stock symbol)*/
	public void subscribe(User u, String product) {
		if (publisher != null) {
			publisher.subscribe(u, product);
		}
	}

	/*unsubscribes users to a product
	 * @param User u (the users name)
	 * @param String product (the stock symbol)*/
	public synchronized void unSubscribe(User u, String product) throws InvalidData {
		if (publisher != null) {
			publisher.unSubscribe(u, product);
		}
	}

	// returns an instance of a MessagePublisher	
	public synchronized static MessagePublisher getInstance(){
		if(instance ==null){
			instance = new MessagePublisher();
			publisher = PublisherFactory.makePublisher();
			return instance;
		}
		else
			return instance;
	}

	/*publishes a cancel message to all users subscribed to the stock of the cancel message
	 * @param CancelMessage cm
	 * */
	public synchronized void publishCancel(CancelMessage cm) throws InvalidData  {
		HashMap<String, ArrayList<User>> sub =  publisher.getSubscribers();
		String product = cm.getProduct();
		ArrayList<User> u;
		if (sub.get(product) != null){
			u = sub.get(product);
			for (User user: u){
				if (user.getUserName().equals(cm.getUser()))
					user.acceptMessage(cm);
			}
		}
		else System.out.println("Cancel Message: there are no subscribers for this product");
	}
	/*publishes a fill message to all users subscribed to the stock of the fill message
	 * @param FillMessage fm
	 * */
	public synchronized void publishFill(FillMessage fm) throws InvalidPriceOperation, InvalidData {
		HashMap<String, ArrayList<User>> sub =  publisher.getSubscribers();
		String product = fm.getProduct();
		ArrayList<User> u; 
		if (sub.get(product) != null){
			u = sub.get(product);
			for (User user: u)
				if (user.getUserName().equals(fm.getUser()))
					user.acceptMessage(fm);	
		}
		else System.out.println("Fill Message: there are no subscribers for this product");
	}
	/*publishes a market message to all Users
	 * @param MarketMessage mm
	 * */
	public synchronized void publishMarketMessage(MarketMessage mm) throws InvalidData  {
		ArrayList <User> users = publisher.getUsers();
		if (users.size() == 0)
			System.out.println("Market Message: subscriber list is empty");
		ArrayList <User> unique = removeDuplicates(users);
		for (User u: unique){
			u.acceptMarketMessage(mm.toString());
		}
	}

	/*checks to remove any duplicate users in the User ArrayList
	 *If noDuplicates already contains the user it is not added to the new Array  
	 *@ param ArrayList<User> users (contains an ArrayList of users subscribed to any stock)
	 */
	private static ArrayList <User> removeDuplicates(ArrayList<User> users){
		ArrayList<User> noDuplicates = new ArrayList<>();
		for (User u: users)
			if (!(noDuplicates.contains(u))){
				noDuplicates.add(u);
			}
		return noDuplicates;
	}


}




