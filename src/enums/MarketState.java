package enums;

public enum MarketState {
	CLOSED("CLOSED"), OPEN("OPEN"), PREOPEN("PREOPEN");

	private String marketState;




	private MarketState(String s) {
		String upper = s.toUpperCase();
		if (upper.equals("CLOSED") || upper.equals("OPEN") || upper.equals("PREOPEN") )
			this.marketState = upper;
	}
	public String getState(){
		return marketState;
	}
	
	

}




