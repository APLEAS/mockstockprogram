package enums;

public enum BookSide {

	BUY("BUY"), SELL("SELL");
	private String bookSide;




	private BookSide(String s) {
		String upper = s.toUpperCase();
		if (upper.equals("BUY") || upper.equalsIgnoreCase("SELL") )
			bookSide = upper;
	}
	public String getSide(){
		return bookSide;
	}
	//else {throw new RunTimeInputException("RunTimeInputException:  Bookside string must be Buy or Sell");
}