/*TradableDTO.java 
Andrew Pleas
SE 450*/

package tradable;

import enums.BookSide;
import price.Price;

public class TradableDTO {
	public String product;
	public Price price;
	public int originalVolume;
	public int remainingVolume;
	public int cancelledVolume;
	public String user;
	public BookSide side;
	public boolean quote;
	public String id; 

	/*	 TradableImple constructor creates a string to print all of the data in a Tradable Object
	 *  @param productSymbol (details about the product)
	 *  @param price (Price object of the Tradable object)
	 *  @param originalVolume (original price of the order)
	 *  @param remainingVolume (remaining price of the order)
	 *  @param cancelledVolume (cancelled volume of the order)
	 *  @param user (name of the User)
	 *  @param side (String indicator either "BUY" or "SELL")
	 *  @param quote (boolean indicator of "BUY" or "SELL")
	 *  @param id (id of the Tradable object)	
	 */
	public TradableDTO(String product, Price price, int originalVolume,
			int remainingVolume, int cancelledVolume, String user, BookSide bookSide,
			boolean quote, String id) {

		this.product = product;
		this.price = price;
		this.originalVolume = originalVolume;
		this.remainingVolume = remainingVolume;
		this.cancelledVolume = cancelledVolume;
		this.user = user;
		this.side = bookSide;
		this.quote = quote;
		this.id = id;

	}

	//creates a string that prints all of the data in a Tradable object
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("Product: %s, Price: %s", product, price.toString()));
		sb.append(String.format(", OriginalVolume: %s, RemainingVolume: %s", originalVolume, remainingVolume));
		sb.append(String.format(", CancelledVolume: %s, User: %s", cancelledVolume, user));
		sb.append(String.format(", Side: %s, IsQuote: %b, Id: %s", side, quote, id));
		return sb.toString();
	}


}
