/*TradableImple.java 
Andrew Pleas
SE 450*/

package tradable;

import enums.BookSide;
import exceptions.InvalidData;
import price.Price;


public  class TradableImpl implements Tradable {
	private String userName;
	private String productSymbol;
	private String id;
	private BookSide side;
	private Price price;
	private String orderVolume;
	private String remainingVolume;
	private int originalOrder;
	private int remainingOrder;
	private int cancelledOrder;

	/*TradableImple implements Tradable
	 * TradableImple constructor creates a Tradable object
	@param userName (name of the User)
	@param productSymbol (details about the product)
	@param price (Price object of the Tradable object)
	@param originalOrder (original price of the order)
	@param bookSide (String indicator either "BUY" or "SELL")*/

	TradableImpl(String userName, String productSymbol, Price price, int originalOrder,
		BookSide bookSide) throws Exception{		 
		setUser(userName);
		setProduct(productSymbol);
		setPrice(price);
		setSide(bookSide);
		setOrderVolume(originalOrder);
		setRemainingVolume(originalOrder);
		setId(userName, productSymbol, price);

	}
	/* sets the productSymbol of the Tradable object
	 * @param newProduct (productSymbol equals newProduct)*/ 

	private void setProduct(String newProduct) throws InvalidData{
		if (newProduct == null) throw new InvalidData("InvalidData: Product Symbol cannot be null");
		if (newProduct.isEmpty()) throw new InvalidData("InvalidData: Product cannot be empty");
		productSymbol = newProduct;
	}

	//returns the product symbol of the Tradable object
	public String getProduct(){
		return productSymbol;
	}
	/* sets the Tradable objects Price
	 * @param newPrice (price equals newPrice)*/ 
	public void setPrice(Price newPrice) throws InvalidData{
		if(newPrice == null)
			throw new InvalidData("InvalidData: Price cannot be null");
		price = newPrice;
	}
	
	//returns a Price object for the Tradable object.
	public Price getPrice(){
		return price;
	}
	

	//returns the original order volume for a Tradable object
	public int getOriginalVolume(){
		return originalOrder;
	}

	//returns the remaining order volume for a Tradable object
	public int getRemainingVolume(){
		return remainingOrder;
	}
	//returns the cancelled order volume for a Tradable object
	public int getCancelledVolume(){
		return cancelledOrder;
	}

	/*		Sets the order volume for a Tradable object
	 * @param newOrderVolume (originalOrder equals newOrderVolume)
		Throws InvalidData exception newOrderVolume <=0*/	
	private void setOrderVolume( int newOrderVolume)throws InvalidData{
		if (newOrderVolume <= 0) throw new InvalidData("InvalidData: Order price must be greater than 0");
		originalOrder = newOrderVolume;
		String oVol = Integer.toString(newOrderVolume);
		orderVolume = oVol;
	}

	/*		sets the cancelled volume for a Tradable object
	 * @param newCancelledVolume (cancelledORder = newCancelledVolume)
		Throws InvalidData exception newCancelledVolume > originalOrder*/
	public void setCancelledVolume(int newCancelledVolume) throws InvalidData{
		if (newCancelledVolume > originalOrder) throw new InvalidData("Requested new Cancelled Volume (" + newCancelledVolume + ") exceeds the tradable's Original Volume ("+ getOriginalVolume() + ")");
		else cancelledOrder = newCancelledVolume;
	}
	/*		sets the remaining volume for a Tradable object
	 * @param newRemainingVolume (remainingVolume = newRemainingVolume)
		Throws InvalidData exception newRemainingVolume > originalOrder*/
	public void setRemainingVolume( int newRemainingVolume)throws InvalidData{
		if (newRemainingVolume > originalOrder) throw new InvalidData("Requested new Remaining Volume (" + newRemainingVolume + ") exceeds the tradable's Original Volume ("+ getOriginalVolume() + ")");
		remainingOrder = newRemainingVolume;
		String rVol = Integer.toString(newRemainingVolume);
		remainingVolume = rVol;
	}
	//returns the user name for a Tradable object
	public String getUser(){
		return userName;
	}
	/* sets the Tradable objects userName
	 * @param newUserName (UserName equals newUserName)
	 * Throws InvalidData exception when newUserName is Null*/ 
	private void setUser(String newUserName) throws InvalidData{ 
		if (newUserName == null) throw new InvalidData("InvalidData: User Name cannot be null");
		if (newUserName.isEmpty()) throw new InvalidData("InvalidData: User Name cannot be empty");
		userName = newUserName;
	}
	/* sets the Tradable objects Side
	 * @param newSide (side equals newSide)
	 * Throws InvalidData exception when newSide is Null*/
	private void setSide(BookSide newSide) throws InvalidData{
		if (newSide == null) throw new InvalidData("InvalidData: Side cannot be null");
		side = newSide;
	}
	//returns the buy or sell side of a Tradable object
	public BookSide getSide(){	
		return side;
	}

	//returns true if the Tradable object is a Quote
	public boolean isQuote(){ 
		if(this.getClass().toString().contains("Quote")) return true;
		else return false;
	}
	// returns a string representation of a Tradable object
	@Override
	public String getId() {
		//	String idd = getUser() + getProduct() + price.toString()+ System.nanoTime();
		return id;
	}
	/* sets the id of the Tradable object to String form  
	 * @param user 
	 * @param product
	 * @param p 
	 * The sytem time is appended to the end of the string*/ 
	private void setId(String user, String product, Price p) {
		long time = System.nanoTime();
		id = user + product + p.toString() + time;//+ time;
	}

	//returns a string representation of a Tradable object
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("%s order: %s %s %s at %s", userName, side, remainingVolume, productSymbol, price.toString()));
		sb.append(String.format(" (Original Vol: %s, CXL'd Vol: %s), ID: %s", orderVolume, cancelledOrder, id));
		return sb.toString();
	}





}
