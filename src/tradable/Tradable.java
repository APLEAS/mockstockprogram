/*Tradable.java 
Andrew Pleas
SE 450*/

package tradable;

import enums.BookSide;
import exceptions.InvalidData;
import price.Price;

public interface Tradable {

	String getProduct();

	Price getPrice();

	int getOriginalVolume();

	int getRemainingVolume();

	int getCancelledVolume();

	void setCancelledVolume(int newCancelledVolume) throws InvalidData;

	void setRemainingVolume( int newRemainingVolume) throws InvalidData;

	String getUser();

	BookSide getSide();
	
	boolean isQuote();

	String getId();

	String toString();

	void setPrice(Price price) throws InvalidData;
}
