/*Quote.java 
Andrew Pleas
SE 450*/

package tradable;
import exceptions.InvalidData;
import price.Price;
import enums.BookSide;


public class Quote {
	private String userName;
	private String productSymbol;
	private QuoteSide buyQuote;
	private QuoteSide sellQuote;
	private Price sellPrice;
	private Price buyPrice;

	/*Quote class creates a Quote object 
	 * @param userName the user name for the Quote object 
	 * @param buyPrice the buy price for the Quote object
	 * @param buyVolume the buy volume for the Quote object
	 * @param sellPrice the sell price for the Quote object
	 * @param sellVolume the sell Volume for the Quote object
	 */


	public Quote(String userNameIn, String productSymbolIn, Price buyPriceIn, int buyVolumeIn, Price sellPriceIn, int sellVolumeIn) throws Exception{
		setSellPrice(sellPriceIn);
		setBuyPrice(buyPriceIn);
		setUser(userNameIn);
		setProduct(productSymbolIn);
		setBuy(userNameIn, productSymbolIn, buyPriceIn, buyVolumeIn);
		setSell(userNameIn, productSymbolIn, sellPriceIn, sellVolumeIn);
		
	}
	private void setBuyPrice(Price newBuyPrice) throws InvalidData {
		if (newBuyPrice.getPrice() < 0 || newBuyPrice == null){
			System.out.println("negaitve newBuyPrice  =  " + newBuyPrice.getPrice());
			throw new InvalidData("InvalidData : (Quote setbuyPrice) buyPrice cannot be less than 0 or null");
		}
		buyPrice = newBuyPrice;
	}
	
	private void setSellPrice(Price newSellPrice) throws InvalidData {
		if (newSellPrice.getPrice() < 0 || newSellPrice == null){
			System.out.println("negaitve newSellPrice  =  " + newSellPrice.getPrice());
			throw new InvalidData("InvalidData : (Quote setSellPrice) sellPrice cannot be less than 0 or null");
		}
		sellPrice = newSellPrice;
		
	}
	public Price getSellPrice(){
		return sellPrice;
	}
	
	public Price getBuyPrice(){
		return buyPrice;
	}
	/*public int getSellVolume(){
		return 
	}*/
	public void setUser(String newUserName) throws InvalidData{ 
		if (newUserName == null) throw new InvalidData("InvalidData: User Name cannot be null");
		if (newUserName.isEmpty()) throw new InvalidData("InvalidData: UserName cannot be empty");
		userName = newUserName;
	}

	public String getUserName(){
		return userName;

	}
	public void setProduct(String newProduct) throws InvalidData{
		if (newProduct == null) throw new InvalidData("InvalidData: Product Symbol cannot be null");
		if (newProduct.isEmpty()) throw new InvalidData("InvalidData: Product cannot be empty");
		productSymbol = newProduct;
	}
	public String getProduct(){
		return productSymbol;
	}
	private void setBuy(String userName, String product, Price buyPrice, int buyVolume) throws Exception{
		if (buyVolume < 0) throw new InvalidData("Buy Price can not be negative");
		buyQuote = new QuoteSide(userName, product, buyPrice, buyVolume, BookSide.BUY);
	}

	private void setSell(String userName, String product, Price sellPrice, int sellVolume) throws Exception{
		if (sellVolume < 0) throw new InvalidData("Sell Price can not be negative");
		sellQuote = new QuoteSide(userName, product, sellPrice, sellVolume, BookSide.SELL);
	}

	public QuoteSide getQuoteSide(BookSide sideIn) throws Exception {
		if(sideIn == BookSide.BUY){
			QuoteSide buy = new QuoteSide(buyQuote);
			return buy;
		}
		else {
			QuoteSide sell = new QuoteSide(sellQuote);
			return sell;
		}
		
	}

	public String toString(){
		StringBuilder quoteString = new StringBuilder();
		quoteString.append(String.format("%s quote: %s %s - %s", userName, productSymbol, buyQuote.toString(), sellQuote.toString()));
		return quoteString.toString();

	}

}
