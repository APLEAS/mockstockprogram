/*QuoteSide.java 
Andrew Pleas
SE 450*/

package tradable;

import enums.BookSide;
import price.Price;

public class QuoteSide extends TradableImpl implements Tradable {

	/*QuoteSide constructor creates a QuoteSide Object
	 * @param userName (name of the User)
	 * @param product (details about the product)
	 * @param orderPrice (price of the order)
	 * @param originalOrder (original price of the order)
	 * @param bookSide (String indicator either "BUY" or "SELL")
	 */

	public QuoteSide(String userName, String productSymbol, Price orderPrice, int originalOrder,
			BookSide bookSide) throws Exception {

		super(userName, productSymbol, orderPrice, originalOrder, bookSide);

	}
	
	/*QuoteSide constructor creates a QuoteSide Object from a QuoteSide object.
	 */
	public QuoteSide(QuoteSide quote) throws Exception{
		super(quote.getUser(), quote.getProduct(), quote.getPrice(), quote.getOriginalVolume(), quote.getSide());
	}

	//toString() creates a string representation of a QuoteSide object
	@Override
	public String toString() {
		StringBuilder quoteString = new StringBuilder();
		quoteString.append(String.format("%s x %s (Original vol: %s, CXL'd Vol: %s)  %s",getPrice().toString(), getRemainingVolume(), getOriginalVolume(), getCancelledVolume(), getId()));
		return quoteString.toString();
	}
}

