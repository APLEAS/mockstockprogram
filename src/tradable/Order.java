/*Order.java 
Andrew Pleas
SE 450*/

package tradable;
import enums.BookSide;
import price.Price;

//The Order class makes a buy or a sell Order object

public class Order extends TradableImpl implements Tradable {

	/*Order constructor creates an order object
@param userName (name of the User)
@param product (details about the product)
@param orderPrice (price of the order)
@param originalOrder (original price of the order)
@param bookSide (String indicator either "BUY" or "SELL")*/

	public Order(String userName, String productSymbol, Price orderPrice, int originalOrder,
			BookSide bookSide) throws Exception {
		super(userName, productSymbol, orderPrice, originalOrder, bookSide);
	}


}


