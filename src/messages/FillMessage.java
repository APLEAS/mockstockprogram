/*FillMessage.java 
Andrew Pleas
SE 450*/

package messages;

import exceptions.InvalidData;
import price.Price;
import enums.BookSide;

public class FillMessage extends MessageImpl implements Comparable<FillMessage>{

	public FillMessage(String user, String product, Price price, int volume,
			String details, BookSide side, String id) throws InvalidData {
		super(user, product, price, volume, details, side, id);

	}
	//compares the cancel message price to the price of the stock
	//returns -1 if there is an exception
	public int compareTo(FillMessage fillMessage) {
		try{
			Price price = getPrice();
			Price fill = fillMessage.getPrice();
			return price.compareTo(fill);
		} catch (Exception ex){
			System.out.println(ex.getMessage());
			return -1;
		}
	}
	
	//creates and returns a string representation of a Fill Message
	public String toString(){
		StringBuilder fillString = new StringBuilder();
		fillString.append(String.format("User: %s Product: %s, Price: %s, Volume: %s, Details: %s, Side: %s", getUser(), getProduct(), getPrice().toString(), getVolume(), getDetails(), getSide()));
		return fillString.toString();

	}

}




