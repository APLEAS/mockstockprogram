/*Message.java 
Andrew Pleas
SE 450*/

package messages;

import price.Price;
import enums.BookSide;

public interface Message {

	String getProduct();

	Price getPrice();

	int getVolume();

	String getDetails();

	BookSide getSide();

	String getId();
	



}
