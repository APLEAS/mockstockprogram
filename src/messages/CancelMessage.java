package messages;

import exceptions.InvalidData;
import price.Price;
import enums.BookSide;;


public class CancelMessage extends MessageImpl implements Comparable<CancelMessage> {
	
	public CancelMessage(String user, String product, Price price, int volume,
			String details, BookSide side, String id) throws InvalidData {
		super(user, product, price, volume, details, side, id);

	}

	//compares the cancel message price to the price of the stock
	//returns -1 if there is an exception
	public int compareTo(CancelMessage cancelMessage) {
		try{
		Price price = getPrice();
		Price cancel = cancelMessage.getPrice();
		return price.compareTo(cancel);
	} catch (Exception ex){
		System.out.println(ex.getMessage());
		return -1;
	}
}
	//creates and returns a string representation of a Cancel Message
	public String toString(){
		StringBuilder cancelString = new StringBuilder();
		cancelString.append(String.format("User: %s Product: %s, Price: %s, Volume: %s, Details: %s, Side: %s, id: %s", getUser(), getProduct(), getPrice().toString(), getVolume(), getDetails(), getSide(), getId()));
		return cancelString.toString();

	}

}
	

