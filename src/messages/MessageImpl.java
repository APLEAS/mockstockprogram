/*MessageImpl.java 
Andrew Pleas
SE 450*/

package messages;

import exceptions.InvalidData;
import price.Price;
import enums.BookSide;

public class MessageImpl implements Message{
	private String user;

	private String product;

	private Price price;

	private int volume;

	private String details;

	private BookSide side;

	public String id;

	public MessageImpl (String user, String product, Price price, int volume, String details, BookSide side, String id) throws InvalidData{	

		setUser(user);
		setProduct(product);
		setPrice(price);
		setVolume(volume);
		setDetails(details);
		setSide(side);
		setId(id);
	}

	public String getUser(){
		return user;
	}
	/* sets the Message objects userName
	 * @param newUserName (UserName equals newUserName)
	 * Throws InvalidData exception when newUserName is Null*/ 
	private void setUser(String newUserName) throws InvalidData{ 
		if (newUserName == null) throw new InvalidData("InvalidData: User Name cannot be null");
		if (newUserName.isEmpty()) throw new InvalidData("InvalidData: User Name cannot be empty");
		user = newUserName;
	}

	/* sets the productSymbol of the Message object
	 * @param newProduct (productSymbol equals newProduct)*/ 

	private void setProduct(String newProduct) throws InvalidData{
		if (newProduct == null) throw new InvalidData("InvalidData: product cannot be null");
		if (newProduct.isEmpty()) throw new InvalidData("InvalidData: product cannot be empty");
		product = newProduct;
	}

	public String getProduct(){
		return product;
	}

	/* sets the Message objects Price
	 * @param newPrice (price equals newPrice)*/ 
	private void setPrice(Price newPrice){
		price = newPrice;
	}

	public Price getPrice(){
		return price;
	}

	public int getVolume(){
		return volume;
	}

	/* sets the Message objects volume
	 * @param volume (volume of the stock)*/ 
	public int setVolume(int newVolume){
		volume = newVolume;
		return volume;
	}
	/* sets the Message objects details
	 * @param newDetails (details of the stock trade)*/ 
	public String setDetails(String newDetails){
		details = newDetails;
		return details;
	}

	public String getDetails(){
		return details;
	}

	public BookSide getSide(){
		return side;
	}

	/* sets the id of the Tradable object to String form  
	 * @param user 
	 * @param product
	 * @param p 
	 * The sytem time is appended to the end of the string*/ 
	private void setId(String newId) {
		id = newId;
	}

	/* sets the Tradable objects Side
	 * @param newSide (side equals newSide)
	 * Throws InvalidData exception when newSide is Null*/
	private void setSide(BookSide newSide) throws InvalidData{
		if (newSide == null) throw new InvalidData("InvalidData: Side cannot be null");
		side = newSide;
	}

	public String getId(){
		return id;
	}






}
