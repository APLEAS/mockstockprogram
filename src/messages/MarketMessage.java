/*MarketMessage.java 
Andrew Pleas
SE 450*/

package messages;

import enums.MarketState;
import exceptions.InvalidData;

public class MarketMessage  {

	private MarketState state;

	public MarketMessage(String state) throws InvalidData {
		setMarketState(state);

	}
	//sets MarketState to whatever string s is
	//@param s (String can be either "OPEN", "CLOSED", or "PREOPEN")
	private void setMarketState(String s) throws InvalidData{
		if(s == null || s.isEmpty())
			throw new InvalidData("InvalidData: (MarketMessage setmarketState) String s cannot be null or empty");
		boolean check = false;
		String upper = s.toUpperCase();
		if (upper.equalsIgnoreCase("CLOSED")){
			state = MarketState.CLOSED;
			check = true;
		}
		if (upper.equalsIgnoreCase("OPEN")){
			state = MarketState.OPEN;
			check = true;
		}
		if (upper.equalsIgnoreCase("PREOPEN")){
			state = MarketState.PREOPEN;
			check = true;
		}
		if (check == false) 
			throw new InvalidData("Invalid Data: (in MarketMessenger) Market State string must be CLOSED, OPEN, or PREOPEN" + "  " + state.toString());
	}

	//@Override
	//creates a string representation of the MarketState;
	public String toString() {
		switch (state.getState()) {
		case "OPEN":
			return ("OPEN");
		case "PREOPEN":
			return ("PREOPEN");
		default:
			return ("CLOSED");
		}
	}

}
