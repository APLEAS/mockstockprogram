
/*Andrew Pleas
SE 450*/

package messages;
import price.Price;

public class MarketDataDTO {
	public  String product;
	public  Price buyPrice;
	public int buyVolume;
	public  Price sellPrice;
	public  int sellVolume;

	public MarketDataDTO(String product, Price buyPrice, int buyVolume, Price sellPrice, int sellVolume) {
		this.product = product;
		this.buyPrice = buyPrice;
		this.buyVolume = buyVolume;
		this.sellPrice = sellPrice;
		this.sellVolume = sellVolume;
		if (buyPrice == null){
			buyPrice = new Price(0);
		}
		if (sellPrice == null){
			sellPrice = new Price(0);
		}
	}
	
	public String toString(){
	     StringBuilder s = new StringBuilder();
	        s.append(String.format("%s:  %s@%s", product, buyVolume, buyPrice.toString()));
	        s.append(String.format(" X %s@%s,", sellVolume , sellPrice.toString()));
	        return s.toString();
	        

	    }
	}





