/*Price.java 
Andrew Pleas
SE 450*/

package price;
import exceptions.InvalidPriceOperation;
import price.PriceFactory;

public final class Price implements Comparable<Price> {

	/*	boolean variable to distinguish if a price is Market value or a Limit
	if it is true it is a Market Price if it is false its a limit price*/

	private final boolean marketPrice;

	//long to store the price of each Price object
	private final long price;

	//constructor for limit prices
	public Price(long p){
		this.price = p;
		this.marketPrice = false;
		//	PriceFactory.makeLimitPrice(p);
	}

	//constructor for Market Prices
	public Price(){
		this.price = 0;
		this.marketPrice = true;

	}
	
	public long getPrice(){
		return price;
	}

	/*	Adds two prices from two Price objects and creates and
	returns a new Price object */

	public Price add(Price p) throws InvalidPriceOperation{
		if (this.isMarket() == true || p.isMarket() == true){
			throw new InvalidPriceOperation("InvalidPriceOperation: Cannot add using Market Price");	
		}
		long x = price;
		long sum = x + p.price; //need to figure out what value is it might be this
		return PriceFactory.makeLimitPrice(sum); 	
	}

	/*	Subtracts prices from two Price Objects and creates and
	returns a new Price object */

	public Price subtract(Price p) throws InvalidPriceOperation{
		if (this.isMarket() == true || p.isMarket() == true){
			throw new InvalidPriceOperation("InvalidPriceOperation: Cannot subtract using Market Price");	
		}
		long x = price;
		long sub = x - p.price; //need to figure out what value is
		return PriceFactory.makeLimitPrice(sub); 	
	}

	/*	Multiplies prices from two Price Objects and creates and
	returns a new Price object */

	public Price multiply(int p) throws InvalidPriceOperation{ //said he wants a int here ???????
		if (this.isMarket() == true || this.isMarket() == true){
			throw new InvalidPriceOperation("InvalidPriceOperation: Cannot multiply using Market Price");	
		}
		long x = this.price;
		long mult = x * p;
		return PriceFactory.makeLimitPrice(mult);
	}

	/*	Compares prices from two Price Objects and returns 0 if the prices are equal, 1 if the price being
	 * passed as a parameter is less than the other price, and returns -1 if the price being passed in the \
	 * parameter is greater than the other price. */

	public int compareTo(Price p){ //have to fix this
		if (this.price == p.price) {return 0;}
		if (this.price > p.price) {return 1;}
		if (this.price < p.price) {return -1;}
		return 0;
	}

	public boolean greaterOrEqual(Price p){
		if (this.isMarket() || p.isMarket()) return false;
		if (this.price >= p.price) return true;
		else return false;

	}
	public boolean greaterThan(Price p){
		if (this.isMarket() || p.isMarket()) return false;
		if (this.price > p.price) return true;
		else return false;

	}

	public boolean lessOrEqual(Price p){
		if (this.isMarket() || p.isMarket()) return false;
		if (this.price <= p.price) return true;
		else return false;

	}
	public boolean lessThan(Price p){
		if (this.isMarket() || p.isMarket()) return false;
		if (this.price < p.price) return true;
		else return false;
	}
	public boolean equals(Price p){
		if (this.isMarket() || p.isMarket()) return false;
		if (this.price == p.price) return true;
		else return false;
	}

	public boolean isMarket(){ //figure out what to do for Market Price.  
		return this.marketPrice;

	}

	public boolean isNegative(){
		if (0 > this.price) return true;
		else return false;		
	}	

	public String toString(){ // need to finish
		String mktPrice = "MKT";
		double val = (double) price/100;
		if (marketPrice == true)
			return mktPrice;
		String value = String.format("$%,.2f", val);
		return value;
	}


}




