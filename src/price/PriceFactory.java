/*PriceFactory.java 
Andrew Pleas
SE 450*/

package price;

import java.util.HashMap;

public class PriceFactory{
	
	//prices is a hashtable that holds all Price values
	private static HashMap<Long, Price> prices = new HashMap<>();

	//Takes a string value and converts into a limit price object if the price is not already in the hashmap. 
	//if the price is in the hashmap it returns the price object. 
	public static Price makeLimitPrice(String value){
	//	String val = "";
		if (value != null){
		long convert;
		double val;
		String replace = value.replaceAll("[^\\d.]", "");
		val = Double.parseDouble(replace);
		val = Math.round(val *100);
		if (value.contains("-")){
			val = val*-1;
		}
		convert = (new Double(val).longValue()); 
		if (!prices.containsKey(convert)){
			Price limit = makeLimitPrice(convert);
			return limit;
		}

		else return prices.get(convert);
		}
		else{
		value = "0.00";
	return makeLimitPrice(value);
		}
	}
// Makes a Limit Price and stores it in a hashtable 
	public static Price makeLimitPrice(long value){
		if (!prices.containsKey(value)){
			Price limit = new Price(value);
			prices.put(value, limit);
			return limit;
		}
		else return prices.get(value);
	}

	//makes a marketPrice and returns it
	public static Price makeMarketPrice(){

long nOne = -1;
if (!prices.containsKey(nOne)){
Price p = new Price();
prices.put(nOne, p);
return p;
}else return prices.get(nOne);

	}
}




