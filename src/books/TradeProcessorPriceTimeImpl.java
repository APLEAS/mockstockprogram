package books;

import java.util.ArrayList;
import java.util.HashMap;

import price.Price;
import price.PriceFactory;
import exceptions.InvalidData;
import tradable.Tradable;
import books.ProductBookSide;
import messages.FillMessage;

public class TradeProcessorPriceTimeImpl implements TradeProcessor{
	private HashMap<String, FillMessage> fillMessages = new HashMap<String, FillMessage>();
	private ProductBookSide side;

	public TradeProcessorPriceTimeImpl(ProductBookSide sideIn) {
		setProductBookSide(sideIn);
	}

	private void setProductBookSide(ProductBookSide sideIn){
		setSide(sideIn);

	}
	private void setSide(ProductBookSide newSide) {
		side = newSide;

	}

	private String makeFillKey(FillMessage fm) {
		String fillMessage = fm.getUser() + fm.getId() + fm.getPrice().toString();
		return fillMessage;
	}

	private boolean isNewFill(FillMessage fm) {
		String key = makeFillKey(fm);
		if(!fillMessages.containsKey(key))
			return true;
		FillMessage oldFill = fillMessages.get(key);
		if(oldFill.getSide() != fm.getSide()) 
				return true;	
		String oldId = oldFill.getId();
		String newId = fm.getId();
		if(!oldId.equals(newId))
			return true;
		else{
			return false;
		}



	}

	private void addFillMessage(FillMessage fm) {
		if(isNewFill(fm)){
			String key = makeFillKey(fm);
			fillMessages.put(key, fm);
		}
		else{
			String k = makeFillKey(fm);
			FillMessage f = fillMessages.get(k);
			f.setVolume(f.getVolume() + fm.getVolume());
			f.setDetails(fm.getDetails());
		}


	}
	public HashMap<String, FillMessage> doTrade(Tradable trd) throws InvalidData  {
		if(trd == null)
			throw new InvalidData("InvalidData: (TradeProcessorPriceTimeImpl doTrade) Tradable cannot be null");

		fillMessages = new HashMap<String, FillMessage>();
		ArrayList<Tradable> tradedOut = new ArrayList<Tradable>();
		ArrayList<Tradable> entriesAtPrice = side.getEntriesAtTopOfBook();

		for (Tradable t: entriesAtPrice){
				//if(trd.getRemainingVolume()> t.getRemainingVolume() && trd.getRemainingVolume()>0){
			if(trd.getRemainingVolume() - t.getRemainingVolume() >0){
					tradedOut.add(t);
					Price tPrice;
					if(t.getPrice().isMarket()){
						tPrice = trd.getPrice();
					}else
					{
						tPrice = t.getPrice(); }
					int remaining = trd.getRemainingVolume() - t.getRemainingVolume();
					FillMessage tFill = new FillMessage(t.getUser(), t.getProduct(), tPrice, t.getRemainingVolume(), "leaving 0", t.getSide(), t.getId());
					addFillMessage(tFill);
					FillMessage trdFill = new FillMessage(trd.getUser(), trd.getProduct(), tPrice, t.getRemainingVolume(), "leaving" + remaining, trd.getSide(), trd.getId());
					addFillMessage(trdFill);
					trd.setRemainingVolume(trd.getRemainingVolume() - t.getRemainingVolume());
					t.setRemainingVolume(0);
					side.addOldEntry(t);
				
				
			
				}
				 if(trd.getRemainingVolume() <= t.getRemainingVolume()){
					int remainder = t.getRemainingVolume() - trd.getRemainingVolume();
					Price tPrice;
					if(t.getPrice().isMarket()){
					tPrice = trd.getPrice();
					}else{ tPrice = t.getPrice(); }
					FillMessage trdfm = new FillMessage(trd.getUser(), trd.getProduct(), t.getPrice(), trd.getRemainingVolume(), "leaving 0", trd.getSide(), trd.getId());
					addFillMessage(trdfm);
					FillMessage tfm = new FillMessage(t.getUser(), t.getProduct(), t.getPrice(), trd.getRemainingVolume(), "leaving" + remainder, t.getSide(), t.getId());
					addFillMessage(tfm);
					trd.setRemainingVolume(0);
					t.setRemainingVolume(remainder);
					side.addOldEntry(trd);

				}

			
		}
		for (Tradable t: tradedOut){
			entriesAtPrice.remove(t);
		}
		if (entriesAtPrice.isEmpty()){
			side.clearIfEmpty(side.topOfBookPrice());
		}
		return fillMessages;
	}
}




