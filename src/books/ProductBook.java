package books;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import enums.BookSide;
import enums.MarketState;
import exceptions.InvalidData;
import messages.CancelMessage;
import messages.FillMessage;
import messages.MarketDataDTO;
import price.Price;
import price.PriceFactory;
import publisher.LastSalePublisher;
import publisher.MessagePublisher;
import tradable.Order;
import tradable.Quote;
import tradable.QuoteSide;
import tradable.Tradable;
import tradable.TradableDTO;
import exceptions.OrderNotFoundException;
import publisher.CurrentMarketPublisher;

public class ProductBook {
	private String stockSymbol;
	private ProductBookSide buySide;
	private ProductBookSide sellSide;
	private String lastMarket;
	private HashSet<String> userQuotes = new HashSet<>();
	private HashMap<Price, ArrayList<Tradable>> oldEntries = new HashMap< Price, ArrayList<Tradable>>();


	public ProductBook(String stockSymbolIn) throws InvalidData {
		buySide= new ProductBookSide(this, BookSide.BUY);
		sellSide = new ProductBookSide(this, BookSide.SELL);
		setStockSymbol(stockSymbolIn);
		lastMarket = "";
		// TODO Auto-generated constructor stub
	}

	private void setStockSymbol(String s)throws InvalidData{
		if (s == null)
			throw new InvalidData("InvalidData: Stock Symbols cannot be null");
		stockSymbol = s;

	}

	public synchronized ArrayList<TradableDTO> getOrdersWithRemainingQty(String userName)throws InvalidData{
		if(userName == null)
			throw new InvalidData("InvalidData: (ProductBook getOrdersWithRemainingQTY) UserName cannot be null");
		ArrayList<TradableDTO> fullList = new ArrayList<>();
		if(buySide.getOrdersWithRemainingQty(userName) != null){
			ArrayList<TradableDTO> buyT =  buySide.getOrdersWithRemainingQty(userName);
			fullList.addAll(buyT);
		}
		if(sellSide.getOrdersWithRemainingQty(userName) != null){
			ArrayList<TradableDTO> sellT = sellSide.getOrdersWithRemainingQty(userName);
			fullList.addAll(sellT);
		}
		return fullList;

	}

	public synchronized void checkTooLateToCancel(String orderId) throws InvalidData, OrderNotFoundException {
		if (orderId == null)
			throw new InvalidData("InvalidData: (ProductBook checkTooLateToCancel) orderId cannot be null");
		boolean check = false;
		for(Map.Entry<Price, ArrayList<Tradable>> entries : oldEntries.entrySet()){
			ArrayList<Tradable> value = entries.getValue();
			for (Tradable t: value){
				if (t.getId().equals(orderId)){
					CancelMessage cm = new CancelMessage(t.getUser(), t.getProduct(), t.getPrice(), t.getRemainingVolume(),
							"Too late to cancel", t.getSide(), t.getId());
					check = true;
					 MessagePublisher.getInstance().publishCancel(cm);
					 return;
				}

			}
		}
		if (check == false){
			throw new OrderNotFoundException("OrderNotFoundException: The order was not found");
		}
	}

	public synchronized String[ ][ ] getBookDepth() throws InvalidData{
		String[][] bd = new String[2][];
		bd[0] = buySide.getBookDepth();
		bd[1] = sellSide.getBookDepth();
		return bd;
	}

	public synchronized MarketDataDTO getMarketData(){
		int buyVolume  = buySide.topOfBookVolume();;
		int sellVolume = sellSide.topOfBookVolume();;
		Price b = buySide.topOfBookPrice();
		b = priceCheck(b);
		Price s = sellSide.topOfBookPrice();
		s = priceCheck(s);
		MarketDataDTO m = new MarketDataDTO(stockSymbol, b, buyVolume, s, sellVolume);
		return m;

	}

	public synchronized void addOldEntry(Tradable t) throws InvalidData{
		if(t == null)
			throw new InvalidData("InvalidData: (ProductBook addOldEntry) Tradable onjects cannot be null");
		t.setCancelledVolume(t.getRemainingVolume());
		t.setRemainingVolume(0);
		if (!oldEntries.containsKey(t.getPrice())){
			ArrayList<Tradable> trade = new ArrayList<Tradable>();
			trade.add(t);
			oldEntries.put(t.getPrice(), trade);
		}
		if(oldEntries.containsKey(t.getPrice()) )
			oldEntries.get(t.getPrice()).add(t);
	}
	public synchronized void openMarket() throws Exception{
		Price bestBuy = buySide.topOfBookPrice();
		Price bestSale = sellSide.topOfBookPrice();
		bestBuy = priceCheck(bestBuy);
		bestSale = priceCheck(bestSale);	
		while(bestBuy.greaterOrEqual(bestSale) || bestBuy.isMarket() || bestSale.isMarket()){
			if(buySide.getEntriesAtPrice(bestBuy) != null){
			ArrayList<Tradable> topOfBuySide = buySide.getEntriesAtPrice(bestBuy);
			HashMap<String, FillMessage> allFills = new HashMap <String, FillMessage>();
			ArrayList<Tradable> toRemove = new ArrayList <Tradable>();
			for(Tradable t : topOfBuySide){
				allFills = sellSide.tryTrade(t);
				if (t.getRemainingVolume() == 0)
					toRemove.add(t);
			}
			
			for(Tradable t: toRemove){
				buySide.removeTradable(t);
			}
			updateCurrentMarket();
			Price lastSalePrice = determineLastSalePrice(allFills);
			int lastSaleVolume = determineLastSaleQuantity(allFills);
			LastSalePublisher.getInstance().publishLastSale(stockSymbol, lastSalePrice, lastSaleVolume);
			bestBuy = buySide.topOfBookPrice();
			bestSale = sellSide.topOfBookPrice();
			if(bestBuy == null || bestSale == null)
				break;
		}
			return;
		}
	}

	public synchronized  void closeMarket() throws InvalidData, OrderNotFoundException {
		buySide.cancelAll();
		sellSide.cancelAll();
		updateCurrentMarket();
	}

	public synchronized void cancelOrder(BookSide side, String orderId) throws InvalidData, OrderNotFoundException {
		if (side == null)
			throw new InvalidData("InvalidData: (ProductBook cancelOrder) BookSide cannot be null");
		if (orderId == null)
			throw new InvalidData("InvalidData: (ProductBook cancelOrder) OrderId cannot be null");
		if (side.toString() == "BUY")
			buySide.submitOrderCancel(orderId);
		if (side.toString() =="SELL")
			sellSide.submitOrderCancel(orderId);
		updateCurrentMarket();
	}

	public synchronized void cancelQuote(String userName) throws InvalidData {
		buySide.submitQuoteCancel(userName);
		sellSide.submitQuoteCancel(userName);
		updateCurrentMarket();
	}

	public synchronized void addToBook(Quote q) throws Exception, InvalidData{
		if (q == null)
			throw new InvalidData("InvalidData: (ProductBook addToBook) Quote cannot be null");
		QuoteSide buyQuote = q.getQuoteSide(BookSide.BUY);
		QuoteSide sellQuote = q.getQuoteSide(BookSide.SELL);
		Price sellQ = sellQuote.getPrice();
		Price buyQ = buyQuote.getPrice();
		sellQ = priceCheck(sellQ);
		buyQ = priceCheck(buyQ);
		if(sellQ.getPrice() <= buyQ.getPrice())
			throw new InvalidData("InvalidData: Sell Quote cannot be less than or equal to the Buy Quote");
		if(sellQ.getPrice() <= 0 || buyQ.getPrice()<=0){
			
			System.out.println(sellQ.getPrice()  + "    " + buyQ.getPrice());
			throw new InvalidData("InvalidData: Sell Quote and Buy Quote must be greater than 0");
		}
		if(q.getQuoteSide(BookSide.SELL).getOriginalVolume() <= 0 || q.getQuoteSide(BookSide.BUY).getOriginalVolume() <=0)
			throw new InvalidData("InvalidData: Sell Quote and Buy Quote must both have an orignal volume greater than 0");
		if(userQuotes.contains(q.getUserName())){
			buySide.removeQuote(q.getUserName());
			sellSide.removeQuote(q.getUserName());
		}
		addToBook(BookSide.BUY, buyQuote);
		addToBook(BookSide.SELL, sellQuote);
		userQuotes.add(q.getUserName());
		updateCurrentMarket();

	}

	public synchronized void addToBook(Order o) throws Exception {
		if (o == null)
			throw new InvalidData("InvalidData: (ProductBook addToBook) Order cannot be null");
		addToBook(o.getSide(), o);
		updateCurrentMarket();
	}
	public synchronized void updateCurrentMarket() {
		String update = "";
		Price topBuy = buySide.topOfBookPrice();
		Price topSell = sellSide.topOfBookPrice();
		topBuy = priceCheck(topBuy);
		topSell = priceCheck(topSell);
		update = topBuy.toString() + buySide.topOfBookVolume() + topSell.toString() + sellSide.topOfBookVolume();
		if(!lastMarket.equals(update)){
			MarketDataDTO current = new MarketDataDTO(stockSymbol, topBuy, buySide.topOfBookVolume(), topSell, sellSide.topOfBookVolume());
			CurrentMarketPublisher.getInstance().publishCurrentMarket(current);
			lastMarket = update;
		}
	}

	private synchronized Price determineLastSalePrice(HashMap<String, FillMessage> fills) throws InvalidData{
		if (fills == null)
			throw new InvalidData("InvalidData: (ProductBook determineLastSalePrice) HashMap<String, FillMessage> cannot be null");
		ArrayList<FillMessage> msgs = new ArrayList<>(fills.values()); 
		Collections.sort(msgs);
		if(msgs.get(0).getSide() == BookSide.BUY){
			return msgs.get(0).getPrice();
		}
		else{Collections.reverse(msgs);
		return msgs.get(0).getPrice();
		}

	}

	private synchronized int determineLastSaleQuantity(HashMap<String, FillMessage> fills) throws InvalidData {
		if (fills == null)
			throw new InvalidData("InvalidData: (ProductBook determineLastSaleQuantity) HashMap<String, FillMessage> cannot be null");
		ArrayList<FillMessage> msgs = new ArrayList<>(fills.values()); 
		Collections.sort(msgs);
		return msgs.get(0).getVolume();
	}

	private synchronized void addToBook(BookSide side, Tradable trd) throws Exception {
		if (side == null)
			throw new InvalidData("InvalidData: (ProductBook addToBook) BookSide cannot be null");
		if (trd == null)
			throw new InvalidData("InvalidData: (ProductBook addToBook) Tradable object cannot be null");
		if(ProductService.getInstance().getMarketState() == MarketState.PREOPEN){
			if (side == BookSide.BUY){
				buySide.addToBook(trd);
			}
			else if (side == BookSide.SELL){
				sellSide.addToBook(trd);
			}
		}
		else{
			HashMap<String, FillMessage> allFills = null;
			if (side == BookSide.BUY){
				allFills = sellSide.tryTrade(trd);
			}
			if (side == BookSide.SELL){
				allFills = buySide.tryTrade(trd);
			}
			if ((!allFills.isEmpty()) && allFills != null){
				updateCurrentMarket();
				int tradeVolume = trd.getOriginalVolume() - trd.getRemainingVolume();
				Price lastSalePrice = determineLastSalePrice(allFills);
				lastSalePrice = priceCheck(lastSalePrice);
				LastSalePublisher.getInstance().publishLastSale(stockSymbol, lastSalePrice, tradeVolume);

			}
			if(trd.getRemainingVolume() > 0){
				if(trd.getPrice().isMarket()){
					CancelMessage cm = new CancelMessage(trd.getUser(), trd.getProduct(), trd.getPrice(), trd.getRemainingVolume(), "Cancelled", trd.getSide(), trd.getId());
					MessagePublisher.getInstance().publishCancel(cm);

				}

				else if (!trd.getPrice().isMarket()){
					if (side == BookSide.BUY){
						buySide.addToBook(trd);
					}
					else 
						if (side == BookSide.SELL){
							sellSide.addToBook(trd);
						}
				}
			}
		}
	}


	private Price priceCheck(Price p){
		if (p==null){
			p = PriceFactory.makeLimitPrice(0);
			return p;
		}
		else return p;
	}




}









