package books;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import publisher.MessagePublisher;
import messages.MarketDataDTO;
import messages.MarketMessage;
import tradable.Order;
import tradable.Quote;
import tradable.TradableDTO;
import enums.BookSide;
import enums.MarketState;
import exceptions.InvalidData;
import exceptions.OrderNotFoundException;

public class ProductService {
	private volatile static ProductService instance;
	private HashMap<String, ProductBook> allBooks = new HashMap< String, ProductBook >();
	private MarketState state = MarketState.CLOSED;
	ProductService() {
	}

	public synchronized ArrayList<TradableDTO> getOrdersWithRemainingQty(String userName, String product) throws InvalidData{
		if(userName == null || userName.isEmpty())
			throw new InvalidData("InvalidData: (ProductService getOrdersWithRemainingQty) User cannot be null or empty");
		if(product == null || product.isEmpty())
			throw new InvalidData("InvalidData: (ProductService getOrdersWithRemainingQty) Product cannot be null or empty");

		ProductBook book = allBooks.get(product);
		ArrayList<TradableDTO> t = book.getOrdersWithRemainingQty(userName);
		return t;
	}

	public synchronized MarketDataDTO getMarketData(String product) throws InvalidData{
		if(product == null || product.isEmpty())
			throw new InvalidData("InvalidData: (ProductService getMarketData) Product cannot be null or empty");
		if(!allBooks.isEmpty()){
			ProductBook book = allBooks.get(product);
			MarketDataDTO m = book.getMarketData();
			return m;
		}
		else throw new InvalidData("Invalid Data MarketData");
	}

	public synchronized MarketState getMarketState(){
		return state;
	}

	public synchronized String[][] getBookDepth(String product) throws InvalidData {
		if(product == null || product.isEmpty())
			throw new InvalidData("InvalidData: (ProductService getBookDepth) Product cannot be null or empty");
		if (!allBooks.containsKey(product)){
			throw new InvalidData("InvalidData: Product was not found in the book");
		}
		else{
			ProductBook book = allBooks.get(product);
			String[][] depth = book.getBookDepth();
			return depth;
		}


	}
	public synchronized ArrayList<String> getProductList() {
		return new ArrayList<String>(allBooks.keySet());
	}

	public synchronized void setMarketState(MarketState ms) throws Exception{
		if(ms == null)
			throw new InvalidData("InvalidData: (ProductService setmarketState) MarketState cannot be null");
	
	//	String mState = ms.toString().toUpperCase();
	//	if((ms.equals(MarketState.CLOSED) || ms.equals(MarketState.OPEN) || ms.equals(MarketState.PREOPEN))){
			state = ms;
			
			MarketMessage mm = new MarketMessage(ms.toString());
			MessagePublisher.getInstance().publishMarketMessage(mm);
			if (ms.equals(MarketState.OPEN)){
				if(state.equals(MarketState.CLOSED)){
					throw new InvalidData("InvalidData: Market State can only be changed from CLOSED to PREOPEN to OPEN and back to CLOSED");
				}
				for(Entry<String, ProductBook> entries : allBooks.entrySet()){
					ProductBook value = entries.getValue();
					value.openMarket();
					state = ms;
				}
			}
			if (ms.equals(MarketState.CLOSED)){
				if (state.equals(MarketState.PREOPEN)){
					throw new InvalidData("InvalidData: Market State can only be changed from CLOSED to PREOPEN to OPEN and back to CLOSED");
				}
				for(Entry<String, ProductBook> entries : allBooks.entrySet()){
					ProductBook value = entries.getValue();
					value.closeMarket();
					state = ms;
				}
			}
			if (ms.equals(MarketState.PREOPEN)){
System.out.println("made it to preopen");
				if(state.equals(MarketState.OPEN)){
					throw new InvalidData("InvalidData: Market State can only be changed from CLOSED to PREOPEN to OPEN and back to CLOSED");
				}
				state = ms;
			}

	
	}





	public synchronized void createProduct(String product) throws InvalidData{
		if(product == null || product.isEmpty())
			throw new InvalidData("InvalidData: (ProductService createProduct) Product cannot be null or empty");
		if(allBooks.containsKey(product))
			throw new InvalidData("InvalidData: product is allready in the book");
		else{
			ProductBook p = new ProductBook(product);
			allBooks.put(product, p);
		}
	}

	public synchronized void submitQuote(Quote q) throws Exception{
		if(q == null)
			throw new InvalidData("InvalidData: (ProductService submitQuote) Quote cannot be null");
		if (state == MarketState.CLOSED)
			throw new InvalidData("InvalidData: Market is Closed try again later");
		if(!allBooks.containsKey(q.getProduct()))
			throw new InvalidData("InvalidData: Product is not in the book");
		ProductBook value = allBooks.get(q.getProduct());
		value.addToBook(q);


	}

	//}
	public synchronized String submitOrder(Order o) throws Exception{
		if(o == null)
			throw new InvalidData("InvalidData: (ProductService submitOrder) Order cannot be null");
		if (state.toString().equals("CLOSED") )
			throw new InvalidData("InvalidData: Orders cannot be sumbitted when the market is closed");
		if (state.toString().equals("PREOPEN") && o.getPrice().isMarket() )
			throw new InvalidData("InvalidData: An order containing a market price cannot be placed when the Market is PREOPEN");
		if(!allBooks.containsKey(o.getProduct()))
			throw new InvalidData("InvalidData: Product is not in the book");
		else{
		//	for(Entry<String, ProductBook> entries : allBooks.entrySet()){
				ProductBook value = allBooks.get(o.getProduct());
				value.addToBook(o);
			//}
			return o.getId();


		}

	}

	public synchronized void submitOrderCancel(String product, BookSide sideIn, String orderId) throws InvalidData, OrderNotFoundException {
		if(product == null || product.isEmpty())
			throw new InvalidData("InvalidData: (ProductService submitOrderCancel) Product cannot be null or empty");
		if(sideIn == null)
			throw new InvalidData("InvalidData: (ProductService submitOrderCancel) BookSide cannot be null");
		if(orderId == null || orderId.isEmpty())
			throw new InvalidData("InvalidData: (ProductService submitOrderCancel) OrderId cannot be null or empty");
		if (state.toString().equals("CLOSED"))
			throw new InvalidData("InvalidData: Market is Closed try again later");
		if(!allBooks.containsKey(product))
			throw new InvalidData("InvalidData: Product is not in the book");
		else{
			ProductBook value = allBooks.get(product);
			value.cancelOrder(sideIn, orderId);
		}
	}

	public synchronized void submitQuoteCancel(String userName, String product) throws InvalidData {
		if(userName == null || userName.isEmpty())
			throw new InvalidData("InvalidData: (ProductService submitQuoteCancel) UserName cannot be null or empty");
		if(product == null || product.isEmpty())
			throw new InvalidData("InvalidData: (ProductService submitQuoteCancel) Product cannot be null or empty");
		if (state.toString().equals("CLOSED"))
			throw new InvalidData("InvalidData: Market is Closed try again later");
		if(!allBooks.containsKey(product))
			throw new InvalidData("InvalidData: Product is not in the book");
		else{
			ProductBook value = allBooks.get(product);
			value.cancelQuote(userName);
		}
	}

	public static ProductService getInstance() {
		if(instance ==null){
			synchronized(ProductService.class){
			if(instance == null)
			instance = ProductServiceFactory.makeProductService();
			}
		}
		return instance;
	}
}








