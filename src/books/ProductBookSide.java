package books;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import tradable.Order;
import tradable.TradableDTO;
import price.Price;
import price.PriceFactory;
import tradable.Tradable;
import enums.BookSide;
import exceptions.InvalidData;
import exceptions.OrderNotFoundException;
import messages.CancelMessage;
import messages.FillMessage;
import publisher.MessagePublisher;



public class ProductBookSide {
	private BookSide side;
	private HashMap<Price, ArrayList<Tradable>> bookEntries = new HashMap< Price, ArrayList<Tradable>>();
	private TradeProcessor trader;
	private ProductBook parentBook;

	public ProductBookSide(ProductBook book, BookSide sideIn) throws InvalidData {
		setParentBook(book);
		setBookSide(sideIn);
		setTrader();
	}


	private void setTrader() {
		trader = TradeProcessorFactory.makeTradeProcessor(this);
	}


	private void setBookSide(BookSide sideIn) throws InvalidData{
		if(sideIn == null)
			throw new InvalidData("InvalidData: (ProductBookSide setBookSide) BookSide cannot be null");
		side = sideIn;

	}


	private void setParentBook(ProductBook book) throws InvalidData {
		if(book == null)
			throw new InvalidData("InvalidData: (ProductBookSide setParentBook) ProductBook cannot be null");
		parentBook = book;
	}

	public ProductBook getParentBook(){
		return parentBook;
	}
	public BookSide getSide(){
		return side;
	}



	public synchronized ArrayList<TradableDTO> getOrdersWithRemainingQty(String userName) throws InvalidData{
		if(userName == null || userName.isEmpty())
			throw new InvalidData("InvalidData: (ProductBookSide getOrdersWithRemainingQty) userName cannot be null or empty");
		ArrayList<TradableDTO> trade = new ArrayList<TradableDTO>();
		if (!bookEntries.isEmpty()){
			for(Map.Entry<Price, ArrayList<Tradable>> entries : bookEntries.entrySet()){
				ArrayList<Tradable> value = entries.getValue();
				for (Tradable t: value){
					if (t.getUser().equals(userName) && t.getRemainingVolume() > 0 && (!t.isQuote())){
						TradableDTO traderData = new TradableDTO(t.getProduct(), t.getPrice(),
								t.getOriginalVolume(), t.getRemainingVolume(), t.getCancelledVolume(), 
								t.getUser(), t.getSide(), true, t.getId());
						trade.add(traderData);
					}
				}
			}
			return trade;
		}
		return trade;

	}

	synchronized ArrayList<Tradable> getEntriesAtTopOfBook() {
		if (bookEntries.isEmpty()){
			return null;
		}
		else{
			ArrayList<Price> trade = new ArrayList<Price>(bookEntries.keySet());
			Collections.sort(trade);
			if(side == BookSide.BUY){
				Collections.reverse(trade);




			}
			return bookEntries.get(trade.get(0));
		}
	}

	public synchronized String[] getBookDepth() throws InvalidData{
		if (bookEntries.isEmpty())
			return new String[]{"<Empty>"}; 
		else{
			String[] depth = new String[bookEntries.size()];
			ArrayList<Price> trade = new ArrayList<Price>(bookEntries.keySet());
			Collections.sort(trade);

			if(side == BookSide.BUY){
				Collections.reverse(trade);
			}
			int i = 0;

			for(Price p: trade){
				ArrayList<Tradable> t = getEntriesAtPrice(p);
				int sum = 0;
				if(t != null ){
					for(Tradable trader: t){
						sum +=  trader.getRemainingVolume();
					}
					String summary = p + " X " + sum;
					depth[i] = summary;
					i++;
				} 
			}
			return depth;
		}

	}

	synchronized ArrayList<Tradable> getEntriesAtPrice(Price price) throws InvalidData{
		if(price == null)
			throw new InvalidData("InvalidData: (ProductBookSide getEntriesAtPrice) Price cannot be null or empty");
		if(!bookEntries.containsKey(price))
			return null;
		else return bookEntries.get(price);
	}


	public synchronized boolean hasMarketPrice() {
		ArrayList<Price> trade = new ArrayList<Price>(bookEntries.keySet());
		for(Price p: trade)
			if(p.isMarket()){
				return true;
			}

		return false;

	}
	public synchronized boolean hasOnlyMarketPrice(){
		ArrayList<Price> trade = new ArrayList<Price>(bookEntries.keySet());
		for(Price p: trade)
			if(!p.isMarket()){
				return false;
			}

		return true;

	}

	public synchronized Price topOfBookPrice() {
		if(bookEntries.isEmpty()){
			return null;
		}
		else {
			ArrayList<Price> trade = new ArrayList<Price>(bookEntries.keySet());
			Collections.sort(trade);
			if(side == BookSide.BUY){
				Collections.reverse(trade);
			}
			return trade.get(0);
		}
	}

	public synchronized int topOfBookVolume() {
		int sum = 0;
		if(bookEntries.isEmpty())
			return 0;
		else{
			Price p = topOfBookPrice();
			ArrayList <Tradable> t = new ArrayList<Tradable>(bookEntries.get(p));
			for(Tradable trade: t)
				sum += trade.getRemainingVolume();

		}
		return sum;
	}

	public synchronized boolean isEmpty() {
		if(bookEntries.isEmpty())
			return true;
		else return false;
	}
	public synchronized void cancelAll() throws InvalidData, OrderNotFoundException {
		if(!bookEntries.isEmpty()){
			ArrayList<Price> prices = new ArrayList<>(bookEntries.keySet());

			for(Price p : prices){

				ArrayList<Tradable> t = new ArrayList<Tradable>(getEntriesAtPrice(p));
				for(Tradable trade: t){
					if (trade.isQuote()){
						submitQuoteCancel(trade.getUser());


					}else {
						submitOrderCancel(trade.getId());		
					}
				}
			}
		}
	}



	public synchronized TradableDTO removeQuote(String user) throws InvalidData {
		if(user == null || user.isEmpty())
			throw new InvalidData("InvalidData: (ProductBookSide removeQuote) User cannot be null or empty");

		if (!bookEntries.isEmpty()) {
			for(Map.Entry<Price, ArrayList<Tradable>> entries : bookEntries.entrySet()){
				ArrayList<Tradable> value = entries.getValue();
				for (Tradable t: value){
					if (t.getUser().equals(user) && t.isQuote()){
						if(bookEntries.get(t.getPrice()).size() <= 1){
							bookEntries.remove(t.getPrice());
						}
						value.remove(t);
						TradableDTO dto = new TradableDTO(t.getProduct(), t.getPrice(), t.getOriginalVolume(), t.getRemainingVolume(), t.getCancelledVolume(), t.getUser(), t.getSide(), t.isQuote(), t.getId());
						return dto;
					}
				}

			}
		}
		return null;
	}

	public synchronized void submitOrderCancel(String orderId) throws InvalidData, OrderNotFoundException { 
		if(orderId == null || orderId.isEmpty())
			throw new InvalidData("InvalidData: (ProductBookSide submitOrderCancel) OrderId cannot be null or empty");
		if (!bookEntries.isEmpty()) {
			for (Map.Entry<Price, ArrayList<Tradable>> entries : bookEntries.entrySet()) {
				ArrayList<Tradable> value = entries.getValue();
				for (Tradable t : value) {
					if (t.getId().equals(orderId) && (!t.isQuote())) {
						value.remove(t);
						CancelMessage cm = new CancelMessage(t.getUser(), t.getProduct(), t.getPrice(), t.getRemainingVolume(),
								"Cancelled by User:", side, t.getId());
						MessagePublisher.getInstance().publishCancel(cm);
						addOldEntry(t);
						if (value.isEmpty()) {
							bookEntries.remove(t.getPrice()); 
						}
						return; 
					} 
				}
			}
			parentBook.checkTooLateToCancel(orderId); 
		}
	}

	public synchronized void submitQuoteCancel(String userName) throws InvalidData {
		if(userName == null || userName.isEmpty())
			throw new InvalidData("InvalidData: (ProductBookSide submitOrderCancel) UserName cannot be null or empty");
		TradableDTO t = removeQuote(userName);
		if(t!=null){
			CancelMessage cm = new CancelMessage(t.user, t.product, t.price, t.remainingVolume, "Quote " + t.side.toString() + "-Side Cancelled", t.side, t.id);
			MessagePublisher.getInstance().publishCancel(cm);
		}
	}

	public void addOldEntry(Tradable t) throws InvalidData{
		if(t == null)
			throw new InvalidData("InvalidData: (ProductBookSide addOldEntry) Tradable cannot be null");
		parentBook.addOldEntry(t);
	}

	public synchronized void addToBook(Tradable trd) throws Exception{
		if(trd == null)
			throw new InvalidData("InvalidData: (ProductBookSide addToBook) Tradable cannot be null");
		//if(getEntriesAtPrice(trd.getPrice())==null){
		if(!bookEntries.containsKey(trd.getPrice())){
			if(trd.getPrice().isMarket()){
				System.out.println("adding market price  " + trd.getId());
			}
			ArrayList<Tradable> trade = new ArrayList<Tradable>();
			trade.add(trd);
			bookEntries.put(trd.getPrice(), trade);
		}
		else {
			if(trd.getPrice().isMarket()){
				System.out.println("adding market price  " + trd.getId());
			}
			ArrayList<Tradable> t = bookEntries.get(trd.getPrice());
			//t.add(trd);
			//bookEntries.put(trd.getPrice(), t);
			bookEntries.get(trd.getPrice()).add(trd);
		//	t.add(trd);

		}

	}

	public HashMap<String, FillMessage> tryTrade(Tradable trd) throws Exception {
		if(trd == null)
			throw new InvalidData("InvalidData: (ProductBookSide tryTrade) Tradable cannot be null");
		HashMap<String, FillMessage> allFills = new HashMap<String, FillMessage>();
		if (side == BookSide.BUY){
			allFills = trySellAgainstBuySideTrade(trd);
		}
		if (side == BookSide.SELL){
			allFills = tryBuyAgainstSellSideTrade(trd);
		}
		
		
		if(!allFills.isEmpty()){
			Iterator it = allFills.entrySet().iterator();
			while (it.hasNext()){
				Map.Entry f = (Map.Entry) it.next();
				FillMessage fm = (FillMessage) f.getValue();
				MessagePublisher.getInstance().publishFill(fm);
			}
		}
		return allFills;
	}

	public synchronized HashMap<String, FillMessage> trySellAgainstBuySideTrade(Tradable trd) throws Exception{
		if(trd == null)
			throw new InvalidData("InvalidData: (ProductBookSide trySellAgainstBuySideTrade) Tradable cannot be null");
		HashMap<String, FillMessage> allFills = new HashMap<String, FillMessage>();        
		HashMap<String, FillMessage> fillMsgs = new HashMap<String, FillMessage>();
		Price topPrice = priceCheck(topOfBookPrice());
		while((trd.getRemainingVolume() > 0 && !this.isEmpty() && (trd.getPrice().lessOrEqual(topPrice)) || (trd.getRemainingVolume() > 0 && !isEmpty() && trd.getPrice().isMarket()))){
			HashMap<String, FillMessage> temp = trader.doTrade(trd);
			fillMsgs = mergeFills(fillMsgs, temp);

		}
		allFills.putAll(fillMsgs);
		return allFills;
	}

	private HashMap<String, FillMessage> mergeFills(HashMap<String, FillMessage> existing, HashMap<String, FillMessage> newOnes) throws InvalidData {
		if(existing == null)
			throw new InvalidData("InvalidData: (ProductBookSide mergeFills) HashMap<String, FillMessage> 'existing'  cannot be null");
		if(newOnes == null)
			throw new InvalidData("InvalidData: (ProductBookSide mergeFills) HashMap<String, FillMessage> 'newOnes'  cannot be null");

		if(existing.isEmpty())
			return new HashMap<String, FillMessage>(newOnes);
		else{
			HashMap<String, FillMessage> results = new HashMap<String,FillMessage>(existing);
			for (String key : newOnes.keySet())  { 
				if (!existing.containsKey(key))  { 
					results.put(key, newOnes.get(key));
				}else{
					FillMessage fm = results.get(key);
					fm.setVolume(newOnes.get(key).getVolume()); 
					fm.setDetails(newOnes.get(key).getDetails());
				}
			}

			return results;
		}
	}



	public synchronized HashMap<String, FillMessage> tryBuyAgainstSellSideTrade(Tradable trd) throws Exception{
		if(trd == null)
			throw new InvalidData("InvalidData: (ProductBookSide trySellAgainstBuySideTrade) Tradable cannot be null");
		HashMap<String, FillMessage> allFills = new HashMap<String, FillMessage>();         
		HashMap<String, FillMessage> fillMsgs = new HashMap<String, FillMessage>();

		Price topPrice = priceCheck(topOfBookPrice());
		while((trd.getRemainingVolume() > 0 && (!isEmpty()) && trd.getPrice().greaterOrEqual(topPrice)) || ((trd.getRemainingVolume()>0 && 
				(!isEmpty() && trd.getPrice().isMarket())))){
			HashMap<String, FillMessage> temp = trader.doTrade(trd);
			fillMsgs = mergeFills(fillMsgs, temp);
		}
		allFills.putAll(fillMsgs);
		return allFills;
	}


	public synchronized void clearIfEmpty(Price p) throws InvalidData{
		if(p == null)
			throw new InvalidData("InvalidData: (ProductBookSide clearIfEmpty) Price cannot be null");
		if(!bookEntries.isEmpty()){
			if(bookEntries.get(p).isEmpty()){
				bookEntries.remove(p);
			}
		}
	}

	public synchronized void removeTradable(Tradable t) throws InvalidData {
		if(t == null)
			throw new InvalidData("InvalidData: (ProductBookSide removeTradable) Tradable cannot be null");
		ArrayList<Tradable> entries = bookEntries.get(t.getPrice()); 
		if(!entries.isEmpty()){
			boolean check = entries.remove(t);
			if (check){
				if(entries.isEmpty()){
					clearIfEmpty(t.getPrice());
				}
			}
		}

	}
	private Price priceCheck(Price p){
		if (p==null){
			p = PriceFactory.makeLimitPrice(0);
			return p;
		}
		else return p;
	}




}









